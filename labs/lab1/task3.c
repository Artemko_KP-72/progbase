#include <stdio.h>
#include <math.h>

int main(void)
{
    int m = 0;
    int n = 0;
    int j = 0;
    double x = 0;
    double sum1 = 0;
    double sum2 = 0;
    const double PI = 3.1415926535897;

    puts("Enter n value");
    scanf("%i", &n);
    puts("Enter m value");
    scanf("%i", &m);
    printf("n = %i\n", n);
    printf("m = %i\n", m);
    puts("---------------------------------");

    for (int i = 1; i <= n; i++)
    {       sum2 = 0;
            j = 0;
        while( j <= i){
            sum2 = j*j;
            j++;
            printf("i = %2i, j = %2i : sum1 = %5.4lf, sum2 = %5.4lf\n", i, j, sum1, sum2);
        }
        sum1 = sum1 + (sin(2 * PI / (i + 0))) * cos(sum2);
    }
    puts("---------------------------------");
    x = sum1;
    printf("X = %lf\n", x);
}
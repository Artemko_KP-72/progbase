#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define BIG_BUFFER_LENGTH 10000
#define BUFFER_LENGTH 200
#define LINKLEN 100
#define INFLEN 30
#define EPSILON 0.1
#define PERCENT 50
#define SLEEP 125

const char *INPUT = "input.txt";
const char *OUTPUT = "output.txt";

// WEBSITE

struct inf {
  char protocol[INFLEN];
  char domen[INFLEN];
};

struct website {
  char link[LINKLEN];
  int position;
  float daily_visits;
  struct inf site_inf;
};

struct website_node {
  struct website *data;
  struct website_node *next;
};

int webtostr(char *buffer, struct website *point, int bufferlength);
int strtoweb(char *buffer, int bufferlength, struct website *current);
void print_website(struct website *website, int column, int row);
bool nodes_to_file(struct website_node *head);
bool isEqualy(struct website *a, struct website *b);
struct website_node *text_to_nodes(char *buffer, int bufferlength, int amount);
struct website_node *create_SLNode_list(int count);
struct website_node *read_file_to_nodes(const char *fileName,
                                        struct website_node *head,
                                        int node_counts);
bool nodes_to_text(char *buffer, int bufferlength, int amount,
                   struct website_node *head);
void totaly_change_value_function(struct website_node *head);
void find_domen(struct website_node *head);
bool change_value_function(struct website_node *head);
bool change_value(struct website *head, const char *argument, char *new_value);
void totaly_change_value(struct website_node *head, int index);

void titles(void);
bool test(void);
void menu1(void);
void menu2(struct website_node *head);
void cleanBuffer(void);
bool remove_node(struct website_node *head, int index);
void some_additional_function(void);
bool print_all_website(struct website_node *head);
bool add_new_entity(struct website_node *head);

int main(int argc, char *argv[]) {
  if (argc != 1) {
    if (strcmp(argv[1], "-t") == 0) {
      return !test();
    }
    puts("Something is going wrong (\nPlease try again");
    return EXIT_FAILURE;
  }
  menu1();
  return EXIT_SUCCESS;
}

void menu2(struct website_node *head) {
  /*
    Додати у кінець списку нову сутність із вказаними користувачем даними
    Видалити сутність із вказаної позиції у списку
    Перезаписати всі дані полів сутності у вказаній позиції на нововведені
    користувачем (повне оновлення сутності)
    Перезаписати тільки обране поле даних сутності із вказаної позиції у списку
    (часткове оновлення сутності)
    Знайти всі сайти із доменом .com. UPD: Вивести результат у консоль.
    Зберегти поточний список сутностей на файлову систему, запропонувавши
    користувачу ввести назву дискового файлу, у який збережуться всі дані.
*/
  int switcher = 1;
  char local_key = '\0';
  char key = '\0';
  int names_count = 9;
  char names[names_count][BUFFER_LENGTH];
  int nread = 0;
  int step = 0;
  // int count = 0;
  int index = 0;
  char names2[] =
      "Add a new entity to the end of the list with the specified user data#"
      "Delete the entity from the specified position in the list#"
      "Overwrite all entity field data in the specified position on the "
      "user-initiated (full entity update)#"
      "Overwrite only the selected data field of the entity from the specified "
      "position in the list (partial refresh of the entity)#"
      "Find all sites with .com.#"
      "Save the current list of entities to the file system, prompting the "
      "user to enter the name of the disk file, which will store all data.#"
      "Additional function 1#"
      "Additional function 2#"
      "Go back#";

  for (int i = 0; i < names_count; i++) {
    sscanf(names2 + step, "%[^#]%*c%n", names[i], &nread);
    step += nread;
  }
  Console_hideCursor();

  while (key != 'E') {

    Console_clear();

    puts("Welcome to my program\n");
    for (int i = 0; i < names_count; i++) {
      if (switcher == i + 1)
        Console_setCursorAttribute(FG_YELLOW);
      printf("%s\n", names[i]);

      Console_setCursorAttribute(FG_DEFAULT);
    }
    key = Console_getChar();

    if (key == 10) {

      if (switcher == 1) {
        add_new_entity(head);
      }

      if (switcher == 2) {
        Console_clear();
        print_all_website(head);
        Console_getChar();
        local_key = '0';
        while (local_key != '\0') {
          Console_clear();
          puts("\nEnter the item index you want to delete");
          if (!scanf("%i", &index) && (index > 0 && index <= 10)) {
            Console_clear();
            cleanBuffer();
            continue;
          } else
            local_key = '\0';
        }
        if (remove_node(head, index - 1) == false) {
          printf("Something is going wrong");
          Console_getChar();
        }
        print_all_website(head);
        Console_getChar();
      }

      if (switcher == 3) {
        Console_clear();
        totaly_change_value_function(head);
      }

      if (switcher == 4) {
        change_value_function(head);
        Console_getChar();
      }

      if (switcher == 5) {
        Console_clear();
        find_domen(head);
        Console_getChar();
      }

      if (switcher == 6)
        nodes_to_file(head);

      if (switcher == 7)
        titles();

      if (switcher == 8)
        some_additional_function();

      if (switcher == names_count) {
        key = 'E';
        Console_clear();
      }
    }

    if (key == 66)
      switcher++;

    if (key == 65)
      switcher--;

    if (switcher == 0)
      switcher = names_count;

    if (switcher == names_count + 1)
      switcher = 1;
  }
}

bool test(void) {
  struct website_node *head = NULL;
  char big_buffer[BIG_BUFFER_LENGTH];
  head = read_file_to_nodes(INPUT, head, 10);
  struct website somewebsite = {.link = "website",
                                .position = 1,
                                .daily_visits = 18.7,
                                .site_inf = {"https://", ".com"}};
  char str[] = "https:// website .com 1 18.70 ";
  char buffi[BUFFER_LENGTH];
  struct website somewebsite2;

  strtoweb(str, strlen(str), &somewebsite2);
  assert(isEqualy(&somewebsite, &somewebsite2) == true);
  change_value(&somewebsite, "position", "6");
  assert(isEqualy(&somewebsite, &somewebsite2) == true);
  change_value(&somewebsite, "domen", ".ua");

  assert(isEqualy(&somewebsite, &somewebsite2) == false);
  change_value(&somewebsite, "domen", ".com");
  assert(isEqualy(&somewebsite, &somewebsite2) == true);
  change_value(&somewebsite, "position", "1");
  assert(isEqualy(&somewebsite, &somewebsite2) == true);

  webtostr(buffi, &somewebsite2, BUFFER_LENGTH);
  strtoweb(str, strlen(str), &somewebsite2);

  char *fake_buff = NULL;
  assert(nodes_to_text(fake_buff, 30, 4, head) == false);
  struct website_node *fake_head = NULL;
  assert(nodes_to_text(buffi, BUFFER_LENGTH, 4, fake_head) == false);
  assert(nodes_to_text(buffi, BUFFER_LENGTH, 10, head) == true);

  read_file_to_nodes(OUTPUT, head, 10);
  webtostr(buffi, head[3].data, BUFFER_LENGTH);
  assert(strcmp(buffi, "https:// math24 .biz 4 18.70 \n") == 0);
  change_value(head[3].data, "domen", ".ua");
  webtostr(buffi, head[3].data, BUFFER_LENGTH);
  assert(strcmp(buffi, "https:// math24 .biz 4 18.70 \n") != 0);

  strcpy(big_buffer, "https:// website .com 1 18.70 ");
  assert(strtoweb(big_buffer, BIG_BUFFER_LENGTH, head->data) ==
         strlen(big_buffer));
  assert(strcmp(str, big_buffer) == 0);
  assert(remove_node(head, 3) == true);
  assert(strcmp(str, buffi) != 0);

  assert(remove_node(head, 117) == false);
  assert(remove_node(fake_head, 7) == false);
  assert(remove_node(head, 7) == true);
  assert(remove_node(head, 2) == true);
  assert(remove_node(head, 9) == false);

  assert(change_value(head[7].data, "domen", ".ua") == true);
  assert(strcmp(head[7].data->site_inf.domen, ".ua") == 0);
  assert(change_value(head[9].data, "daily_visits", "9.498664546435") == true);
  assert(head[7].data->daily_visits - 9.498665 < EPSILON);

  assert(change_value(head[2].data, "position", "3"));
  assert(head[2].data->position == 3);
  assert(change_value(head[5].data, "link", "some new link") == true);
  assert(strcmp(head[5].data->link, "some new link") == 0);

  assert(change_value(head[1].data, "protocol", "https") == true);
  assert(strcmp(head[1].data->site_inf.protocol, "https") == 0);
  assert(change_value(head[1].data, "daily_visitos", "9") == false);
  assert(change_value(head[1].data, "daily_visits", "9"));
  assert(change_value(head[1].data, "dumen", "9") == false);

  assert(head[1].data->daily_visits == 9.00);
  assert(change_value(head[2].data, "position", "3") == true);
  assert(head[2].data->position == 3);
  assert(change_value(head[2].data, "posushon", "3") == false);
  assert(nodes_to_file(fake_head) == 0);

  free(head);
  return true;
}

void titles(void) {
  struct ConsoleSize console = Console_size();
  Console_hideCursor();
  for (int i = 1; i < console.rows + 18; i++) {
    Console_clear();
    Console_setCursorPosition(i - 18, 50);
    if (i > 18 && i < console.rows + 18)
      printf("                       you will see me again :) ");
    Console_setCursorPosition(i - 16, 50);
    if (i > 16 && i < console.rows + 16)
      printf("        Without them I would not have done laboratory work");
    Console_setCursorPosition(i - 14, 50);
    if (i > 14 && i < console.rows + 14)
      printf("          my computer, my cat, my VScode and Dennis Ritchi");
    Console_setCursorPosition(i - 12, 50);
    if (i > 12 && i < console.rows + 12)
      printf("                 Olya, Nastya, Nadya, Amir, Oleg");
    Console_setCursorPosition(i - 10, 50);
    if (i > 10 && i < console.rows + 10)
      printf("              Artem, Nikita, Misha, Yaroslav, Vitya, Sasha");
    Console_setCursorPosition(i - 8, 50);
    if (i > 8 && i < console.rows + 8)
      printf("                     Dima, Dan, Bogdan, Igor");
    Console_setCursorPosition(i - 6, 50);
    if (i > 6 && i < console.rows + 6)
      printf(
          "  Phadynyak Ruslan Anatoliyevych - the author of idea this project");
    Console_setCursorPosition(i - 4, 50);
    if (i > 4 && i < console.rows + 4)
      printf("               I want to express my gratitude to:");
    Console_setCursorPosition(i - 2, 50);
    if (i > 2 && i < console.rows + 2)
      puts("              I hope you enjoyed using my creature");
    Console_setCursorPosition(i, 50);
    if (i < console.rows)
      puts("                     This is my second lab");
    sleepMillis(500);
  }
}

bool change_value_function(struct website_node *head) {
  int local_key = '0';
  char parameter[BUFFER_LENGTH];
  char new_value[BUFFER_LENGTH];
  int siteindex = 0;
  while (local_key != '\0') {
    Console_clear();
    puts("\nEnter the index of site");
    if (!scanf("%i", &siteindex)) {
      puts("Maybe you entered incorrect index\nPlease try again :)");
      Console_getChar();
      cleanBuffer();
      continue;
    } else {
      local_key = '\0';
    }
  }

  local_key = '0';

  while (local_key != '\0') {
    Console_clear();
    print_website(head[siteindex - 1].data, -1, -1);
    puts("\nEnter the name of the parameter you are want to change");
    scanf("%s", parameter);
    puts("\nEnter the new value");
    scanf("%s", new_value);
    if (!change_value(head[siteindex - 1].data, parameter, new_value)) {
      puts("Maybe you entered incorrect parameter\nPlease try again :)");
      Console_getChar();
      continue;
    } else {
      local_key = '\0';
    }
  }
  print_website(head[siteindex - 1].data, -1, -1);
  return true;
}

void some_additional_function(void) {
  Console_clear();
  conHideCursor();
  int n = 0;
  int i = 0;
  int j = 0;
  struct ConsoleSize console = Console_size();
  int pieceOFSnow[200][400];
  for (i = 0; i < 200; i++) {
    for (j = 0; j < 400; j++) {
      pieceOFSnow[i][j] = -1;
    }
  }
  srand(time(NULL));
  Console_setCursorAttribute(FG_GREEN);
  Console_setCursorAttribute(BG_BLACK);

  while (1) {
    console = Console_size();
    for (j = 0; j < console.columns; j++) {
      for (i = console.rows - 1; i > 0; i--) {
        pieceOFSnow[i][j] = pieceOFSnow[i - 1][j];
        Console_setCursorPosition(i + 1, j + 1);
        if (pieceOFSnow[i][j] == 1 || pieceOFSnow[i][j] == 2)
          printf("%i", pieceOFSnow[i][j] - 1);
        else
          printf(" ");
      }
      n = rand() % 100 + 1;
      if (n <= PERCENT)
        pieceOFSnow[0][j] = rand() % 2 + 1;
      else
        pieceOFSnow[0][j] = -1;
    }
    sleepMillis(SLEEP);
  }
}

bool add_new_entity(struct website_node *head) {
  if (head == NULL)
    return false;
  Console_clear();
  int i = 0;
  do {
    i++;
  } while (head[i].next != NULL);
  if (i >= 10) {
    printf("Maximum size is 10");
    Console_getChar();
    return false;
  } else {
    totaly_change_value(&head[i - 1], i);
  }
  return true;
}

int webtostr(char *buffer, struct website *point, int bufferlength) {
  return snprintf(buffer, bufferlength, "%s %s %s %i %.2lf \n",
                  point->site_inf.protocol, point->link, point->site_inf.domen,
                  point->position, point->daily_visits);
}

int strtoweb(char *buffer, int bufferlength, struct website *current) {
  if (buffer == NULL || current == NULL)
    return false;
  int nread = 0;
  sscanf(buffer, "%[^ ]%*c %[^ ]%*c %[^ ]%*c %i %f %n",
         current->site_inf.protocol, current->link, current->site_inf.domen,
         &current->position, &current->daily_visits, &nread);
  return nread;
}

struct website_node *create_SLNode_list(int count) {
  struct website_node *head = calloc(10, sizeof(struct website_node));
  struct website *data = calloc(10, sizeof(struct website));
  struct website_node *primal = head;
  for (int i = 0; i < count - 1; i++) {
    head[i].data = &data[i];
    head[i].next = &head[i + 1];
  }
  head[count - 1].data = &data[count - 1];
  head[count - 1].next = NULL;
  return primal;
}

void node_free(struct website_node *point) {
  if (point->data != NULL)
    free(point->data);
  if (point->next != NULL)
    free(point->next);
  free(point);
}

struct website_node *text_to_nodes(char *buffer, int bufferlength, int amount) {
  struct website_node *head = create_SLNode_list(amount);
  int step = 0;
  for (int i = -1; head[i].next != NULL; i++) {
    step += strtoweb(buffer + step, bufferlength, head[i + 1].data);
  }
  return head;
}

bool remove_node(struct website_node *head, int index) {

  if (index < 0 || head == NULL)
    return false;

  int count = 0;
  do {
    count++;
  } while (head[count].next != NULL);

  if (index > count)
    return false;
  int i = -1;
  do {
    i++;
    head[i] = head[i + 1];
    head[i].data->position = i + 1;
  } while (head[i].next != NULL);
  return true;
}

bool nodes_to_text(char *buffer, int bufferlength, int amount,
                   struct website_node *head) {
  if (buffer == NULL || head == NULL)
    return false;
  int step = 0;
  int i = -1;
  do {
    i++;
    if (head[i].data != NULL)
      step += webtostr(buffer + step, head[i].data, bufferlength);
  } while (head[i].next != NULL || i == 1000);
  if (i > 1000)
    return false;
  return true;
}

void find_domen(struct website_node *head) {

  int i = -1;
  int zsuv = -1;
  int count_zsuviv = -1;
  do {
    i++;
    if (strcmp(head[i].data->site_inf.domen, ".com") == 0) {
      count_zsuviv++;
      if (count_zsuviv % 3 == 0) {
        zsuv++;
        count_zsuviv = 0;
      }
      print_website(head[i].data, zsuv * 50, 6 * count_zsuviv);
    }
  } while (head[i].next != NULL);
}

struct website_node *read_file_to_nodes(const char *fileName,
                                        struct website_node *head,
                                        int node_counts) {
  head = create_SLNode_list(node_counts);
  FILE *f;
  char big_buffer[BIG_BUFFER_LENGTH];
  if ((f = fopen(INPUT, "rb")) == NULL) {
    printf("Cannot open file.\n");
    exit(EXIT_FAILURE);
  }
  fread(big_buffer, 1, BIG_BUFFER_LENGTH, f);
  int step = 0;
  for (int i = -1; head[i].next != NULL; i++) {
    step += strtoweb(big_buffer + step, BIG_BUFFER_LENGTH, head[i + 1].data);
  }
  return &head[0];
}

void totaly_change_value(struct website_node *new_element, int index) {
  struct website donor;
  char local_key = '0';

  donor.position = index;

  printf("Enter the protocol");
  printf("~> ");
  scanf("%s", donor.site_inf.protocol);
  printf("Enter the link (without protocol and domen, just name)");
  printf("~> ");
  scanf("%s", donor.link);

  printf("Enter domen\n~> ");
  printf("~> ");
  scanf("%s", donor.site_inf.domen);

  local_key = '0';
  while (local_key != '\0') {
    printf("Enter the average visits per day");
    printf("~> ");
    if (!scanf("%f", &donor.daily_visits)) {
      cleanBuffer();
      continue;
    } else
      local_key = '\0';
  }
  puts("");

  new_element->data->position = donor.position;
  new_element->data->daily_visits = donor.daily_visits;
  strcpy(new_element->data->site_inf.domen, donor.site_inf.domen);
  strcpy(new_element->data->site_inf.protocol, donor.site_inf.protocol);
  strcpy(new_element->data->link, donor.link);
  Console_getChar();
}

void totaly_change_value_function(struct website_node *head) {

  char local_key = '0';
  char local_key_helper = '\0';
  struct website donor;
  int index = 0;

  while (local_key != '\0') {
    printf("\nEnter the position\n~> ");
    if (!scanf("%i", &donor.position) || donor.position < 1 ||
        donor.position > 10) {
      Console_clear();
      cleanBuffer();
      printf("\nSorry, something is incorrect, please enter the position "
             "again\n\n      (position shoud be bigger then 0 but lover then "
             "11)\n");
      continue;
    } else {
      printf("\nAre you sure that you want to change this element? (Y/N)\n");
      print_website(head[donor.position - 1].data, -1, -1);
      local_key_helper = Console_getChar();
      if (local_key_helper == 'Y' || local_key_helper == 'y')
        local_key = '\0';
    }
  }

  Console_setCursorPosition(2, 70);
  printf("Enter the protocol");
  Console_setCursorPosition(3, 70);
  printf("~> ");
  scanf("%s", donor.site_inf.protocol);

  Console_setCursorPosition(5, 70);
  printf("Enter the link (without protocol and domen, just name)");
  Console_setCursorPosition(6, 70);
  printf("~> ");
  scanf("%s", donor.link);

  Console_setCursorPosition(8, 70);
  printf("Enter domen\n~> ");
  Console_setCursorPosition(9, 70);
  printf("~> ");
  scanf("%s", donor.site_inf.domen);

  local_key = '0';
  while (local_key != '\0') {
    Console_setCursorPosition(11, 70);
    printf("Enter the average visits per day");
    Console_setCursorPosition(12, 70);
    printf("~> ");
    if (!scanf("%f", &donor.daily_visits)) {
      cleanBuffer();
      continue;
    } else
      local_key = '\0';
  }
  head[index].data = &donor;
  puts("");
  print_website(head[index].data, -1, -1);
  Console_getChar();
}

bool change_value(struct website *head, const char *argument, char *new_value) {
  int react = 0;
  int end_of_argument = strlen(new_value);
  if (strcmp(argument, "link") == 0) {
    strcpy(head->link, new_value);
    head->link[end_of_argument + 1] = '\0';
    react++;
  }
  if (strcmp(argument, "position") == 0) {
    head->position = atoi(new_value);
    react++;
  }
  if (strcmp(argument, "daily_visits") == 0) {
    head->daily_visits = atof(new_value);
    react++;
  }
  if (strcmp(argument, "protocol") == 0) {
    strcpy(head->site_inf.protocol, new_value);
    react++;
  }
  if (strcmp(argument, "domen") == 0) {
    strcpy(head->site_inf.domen, new_value);
    react++;
  }
  if (react == 0)
    return false;
  return true;
}

bool isEqualy(struct website *a, struct website *b) {
  if (strcmp(a->site_inf.protocol, b->site_inf.protocol) == 0 &&
      strcmp(a->link, b->link) == 0 &&
      strcmp(a->site_inf.domen, b->site_inf.domen) == 0 &&
      (fabs(a->daily_visits - b->daily_visits) < EPSILON))
    return true;
  return false;
}

void menu1(void) {
  int switcher = 1;
  char local_key = '\0';
  char key = '\0';
  int names_count = 3;
  char names2[] = "Fill nodes from data store#Create new array#Go back";
  char names[names_count][BUFFER_LENGTH];
  int nread = 0;
  int step = 0;
  int count = 0;
  struct website_node *head = NULL;
  for (int i = 0; i < names_count; i++) {
    sscanf(names2 + step, "%[^#]%*c%n", names[i], &nread);
    step += nread;
  }
  Console_hideCursor();

  while (key != 'E') {

    Console_clear();

    puts("Welcome to my program\n");
    for (int i = 0; i < names_count; i++) {
      if (switcher == i + 1)
        Console_setCursorAttribute(FG_YELLOW);
      printf("%s\n", names[i]);

      Console_setCursorAttribute(FG_DEFAULT);
    }
    key = Console_getChar();

    if (key == 10) {

      if (switcher == 1) {
        head = read_file_to_nodes(INPUT, head, 10);
        menu2(head);
      }

      if (switcher == 2) {
        local_key = '0';
        while (local_key != '\0') {
          puts("\nplease enter the required number of sites");
          if (!scanf("%i", &count)) {
            Console_clear();
            cleanBuffer();
            continue;
          } else
            local_key = '\0';
        }
        head = create_SLNode_list(count);
        menu2(head);
      }
      if (switcher == 3) {
        key = 'E';
        Console_clear();
      }
    }

    if (key == 66)
      switcher++;

    if (key == 65)
      switcher--;

    if (switcher == 0)
      switcher = names_count;

    if (switcher == names_count + 1)
      switcher = 1;
  }
  node_free(head);
}

bool nodes_to_file(struct website_node *head) {
  if (head == NULL)
    return false;
  Console_clear();
  char output[BUFFER_LENGTH];
  char tofile[BIG_BUFFER_LENGTH];
  printf("Enter the output file name\n*if you want use standart out put just "
         "enter \"standart\"\n~> ");
  scanf("%s", output);
  if (strcmp(output, "standart") == 0)
    strcpy(output, OUTPUT);
  nodes_to_text(tofile, BIG_BUFFER_LENGTH, 11, head);
  FILE *f = fopen(output, "wb");
  fprintf(f, "%s", tofile);
  fclose(f);
  return true;
}

bool print_all_website(struct website_node *head) {

  if (head == NULL)
    return false;
  Console_clear();
  int i = -1;
  int zsuv = -1;
  int count_zsuviv = -1;
  do {
    i++;
    count_zsuviv++;
    if (count_zsuviv % 3 == 0) {
      zsuv++;
      count_zsuviv = 0;
    }
    print_website(head[i].data, zsuv * 50, count_zsuviv * 6);
    sleepMillis(40);
  } while (head[i].next != NULL);
  return true;
}

void print_website(struct website *website, int column, int row) {

  puts("");

  if (column != -1)
    Console_setCursorPosition(1 + row, column);
  Console_setCursorAttribute(FG_DEFAULT);
  printf("link is ");
  Console_setCursorAttribute(FG_RED);
  printf("%s%s%s\n", website->site_inf.protocol, website->link,
         website->site_inf.domen);

  if (column != -1)
    Console_setCursorPosition(2 + row, column);
  Console_setCursorAttribute(FG_DEFAULT);
  printf("list position ");
  Console_setCursorAttribute(FG_RED);
  printf("%i\n", website->position);

  if (column != -1)
    Console_setCursorPosition(3 + row, column);
  Console_setCursorAttribute(FG_DEFAULT);
  printf("average number of visitors per day ");
  Console_setCursorAttribute(FG_RED);
  printf("%.2f\n", website->daily_visits);

  if (column != -1)
    Console_setCursorPosition(4 + row, column);
  Console_setCursorAttribute(FG_DEFAULT);
  printf("protocol is ");
  Console_setCursorAttribute(FG_RED);
  printf("%s\n", website->site_inf.protocol);

  if (column != -1)
    Console_setCursorPosition(5 + row, column);
  Console_setCursorAttribute(FG_DEFAULT);
  printf("domen is ");
  Console_setCursorAttribute(FG_RED);
  printf("%s\n", website->site_inf.domen);
  Console_setCursorAttribute(FG_DEFAULT);
}

void cleanBuffer() {
  char c = 0;
  while (c != '\n') {
    c = getchar();
  }
}
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define DEF 1000
    int main(void) {
        srand(time(NULL));
        int num = rand() % 50000;
        system("clear");
        char val[3] = {'A', 'A', 'A'};
        for (int i = 0; i < num; i++) {
            val[2]++;
            if (val[2] > 'Z') {
                val[2] = 'A';
                val[1]++;
            }
            if (val[1] > 'Z') {
                val[1] = 'A';
                val[0]++;
            }
            if (val[0] > 'Z')
                val[0] = 'A';
                Console_setCursorPosition(1, 0);
            printf("%c %c %c", val[0], val[1], val[2]);
            sleepMillis(pow(i/100000000, 1/2));
        }
        return 0;
    }
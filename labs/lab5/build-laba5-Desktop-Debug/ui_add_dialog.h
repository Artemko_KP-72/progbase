/********************************************************************************
** Form generated from reading UI file 'add_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADD_DIALOG_H
#define UI_ADD_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_add_dialog
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QFormLayout *formLayout_2;
    QLineEdit *name_in;
    QLineEdit *domen_in;
    QLineEdit *daily_visits_in;
    QLabel *label_4;
    QLineEdit *author_in;
    QLabel *label_3;
    QLineEdit *position_in;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *add_dialog)
    {
        if (add_dialog->objectName().isEmpty())
            add_dialog->setObjectName(QStringLiteral("add_dialog"));
        add_dialog->resize(481, 464);
        formLayoutWidget = new QWidget(add_dialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(30, 40, 411, 258));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setFamily(QStringLiteral("Norasi"));
        font.setPointSize(16);
        label->setFont(font);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        name_in = new QLineEdit(formLayoutWidget);
        name_in->setObjectName(QStringLiteral("name_in"));
        name_in->setFont(font);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, name_in);

        domen_in = new QLineEdit(formLayoutWidget);
        domen_in->setObjectName(QStringLiteral("domen_in"));
        domen_in->setFont(font);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, domen_in);


        formLayout->setLayout(0, QFormLayout::FieldRole, formLayout_2);

        daily_visits_in = new QLineEdit(formLayoutWidget);
        daily_visits_in->setObjectName(QStringLiteral("daily_visits_in"));
        daily_visits_in->setFont(font);

        formLayout->setWidget(1, QFormLayout::FieldRole, daily_visits_in);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_4);

        author_in = new QLineEdit(formLayoutWidget);
        author_in->setObjectName(QStringLiteral("author_in"));
        author_in->setFont(font);

        formLayout->setWidget(2, QFormLayout::FieldRole, author_in);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        position_in = new QLineEdit(formLayoutWidget);
        position_in->setObjectName(QStringLiteral("position_in"));
        position_in->setFont(font);

        formLayout->setWidget(3, QFormLayout::FieldRole, position_in);

        buttonBox = new QDialogButtonBox(formLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(4, QFormLayout::FieldRole, buttonBox);


        retranslateUi(add_dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), add_dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), add_dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(add_dialog);
    } // setupUi

    void retranslateUi(QDialog *add_dialog)
    {
        add_dialog->setWindowTitle(QApplication::translate("add_dialog", "Dialog", 0));
        label->setText(QApplication::translate("add_dialog", "Link", 0));
        label_2->setText(QApplication::translate("add_dialog", "Daily visits", 0));
        name_in->setText(QApplication::translate("add_dialog", "some site", 0));
        domen_in->setText(QApplication::translate("add_dialog", ".com", 0));
        daily_visits_in->setText(QApplication::translate("add_dialog", "1", 0));
        label_4->setText(QApplication::translate("add_dialog", "Author", 0));
        author_in->setText(QApplication::translate("add_dialog", "Me)", 0));
        label_3->setText(QApplication::translate("add_dialog", "Position", 0));
        position_in->setText(QApplication::translate("add_dialog", "987645", 0));
    } // retranslateUi

};

namespace Ui {
    class add_dialog: public Ui_add_dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADD_DIALOG_H

/********************************************************************************
** Form generated from reading UI file 'errmes.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ERRMES_H
#define UI_ERRMES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_errMes
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *errMes)
    {
        if (errMes->objectName().isEmpty())
            errMes->setObjectName(QStringLiteral("errMes"));
        errMes->resize(308, 127);
        verticalLayoutWidget = new QWidget(errMes);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 261, 101));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setTextInteractionFlags(Qt::LinksAccessibleByKeyboard|Qt::LinksAccessibleByMouse|Qt::TextBrowserInteraction|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        verticalLayout->addWidget(label);

        buttonBox = new QDialogButtonBox(verticalLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(errMes);
        QObject::connect(buttonBox, SIGNAL(accepted()), errMes, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), errMes, SLOT(reject()));

        QMetaObject::connectSlotsByName(errMes);
    } // setupUi

    void retranslateUi(QDialog *errMes)
    {
        errMes->setWindowTitle(QApplication::translate("errMes", "Dialog", 0));
        label->setText(QApplication::translate("errMes", "You have entered incored data", 0));
    } // retranslateUi

};

namespace Ui {
    class errMes: public Ui_errMes {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ERRMES_H

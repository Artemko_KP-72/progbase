/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *save;
    QAction *load_2;
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QPushButton *add;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *load;
    QPushButton *execute;
    QPushButton *remove;
    QPushButton *edit;
    QListWidget *listWidget;
    QFormLayout *formLayout;
    QLabel *link_in;
    QLabel *link_out;
    QLabel *position_in;
    QLabel *position_out;
    QLabel *visits_in;
    QLabel *author;
    QLabel *author_out;
    QLabel *visits_out;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *find_domen_line;
    QPushButton *find;
    QPushButton *close;
    QListWidget *listWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label;
    QLabel *link_out_2;
    QLabel *label_3;
    QLabel *position_out_2;
    QLabel *label_5;
    QLabel *visits_out_2;
    QLabel *label_7;
    QLabel *author_out_2;
    QMenuBar *menuBar;
    QMenu *menuWebsites_Editor;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(577, 700);
        QFont font;
        font.setPointSize(16);
        MainWindow->setFont(font);
        save = new QAction(MainWindow);
        save->setObjectName(QStringLiteral("save"));
        load_2 = new QAction(MainWindow);
        load_2->setObjectName(QStringLiteral("load_2"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(30, 10, 514, 633));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        add = new QPushButton(horizontalLayoutWidget);
        add->setObjectName(QStringLiteral("add"));
        QFont font1;
        font1.setFamily(QStringLiteral("Norasi"));
        font1.setPointSize(18);
        add->setFont(font1);
        add->setCheckable(false);

        verticalLayout_3->addWidget(add);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        load = new QPushButton(horizontalLayoutWidget);
        load->setObjectName(QStringLiteral("load"));
        load->setFont(font1);

        horizontalLayout_2->addWidget(load);

        execute = new QPushButton(horizontalLayoutWidget);
        execute->setObjectName(QStringLiteral("execute"));
        execute->setEnabled(true);
        execute->setFont(font1);

        horizontalLayout_2->addWidget(execute);

        remove = new QPushButton(horizontalLayoutWidget);
        remove->setObjectName(QStringLiteral("remove"));
        remove->setEnabled(false);
        remove->setFont(font1);

        horizontalLayout_2->addWidget(remove);

        edit = new QPushButton(horizontalLayoutWidget);
        edit->setObjectName(QStringLiteral("edit"));
        edit->setEnabled(false);
        edit->setFont(font1);

        horizontalLayout_2->addWidget(edit);


        verticalLayout_3->addLayout(horizontalLayout_2);

        listWidget = new QListWidget(horizontalLayoutWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setEnabled(true);
        listWidget->setMouseTracking(true);
        listWidget->setAcceptDrops(true);

        verticalLayout_3->addWidget(listWidget);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(10, -1, -1, 35);
        link_in = new QLabel(horizontalLayoutWidget);
        link_in->setObjectName(QStringLiteral("link_in"));
        link_in->setFont(font1);

        formLayout->setWidget(3, QFormLayout::LabelRole, link_in);

        link_out = new QLabel(horizontalLayoutWidget);
        link_out->setObjectName(QStringLiteral("link_out"));
        QFont font2;
        font2.setFamily(QStringLiteral("Loma"));
        font2.setPointSize(16);
        link_out->setFont(font2);
        link_out->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(3, QFormLayout::FieldRole, link_out);

        position_in = new QLabel(horizontalLayoutWidget);
        position_in->setObjectName(QStringLiteral("position_in"));
        position_in->setFont(font1);

        formLayout->setWidget(5, QFormLayout::LabelRole, position_in);

        position_out = new QLabel(horizontalLayoutWidget);
        position_out->setObjectName(QStringLiteral("position_out"));
        position_out->setFont(font2);
        position_out->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(5, QFormLayout::FieldRole, position_out);

        visits_in = new QLabel(horizontalLayoutWidget);
        visits_in->setObjectName(QStringLiteral("visits_in"));
        visits_in->setFont(font1);

        formLayout->setWidget(6, QFormLayout::LabelRole, visits_in);

        author = new QLabel(horizontalLayoutWidget);
        author->setObjectName(QStringLiteral("author"));
        author->setFont(font1);

        formLayout->setWidget(7, QFormLayout::LabelRole, author);

        author_out = new QLabel(horizontalLayoutWidget);
        author_out->setObjectName(QStringLiteral("author_out"));
        author_out->setFont(font2);
        author_out->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(7, QFormLayout::FieldRole, author_out);

        visits_out = new QLabel(horizontalLayoutWidget);
        visits_out->setObjectName(QStringLiteral("visits_out"));
        visits_out->setFont(font2);
        visits_out->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        formLayout->setWidget(6, QFormLayout::FieldRole, visits_out);


        verticalLayout_3->addLayout(formLayout);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(600, 30, 451, 561));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        find_domen_line = new QLineEdit(verticalLayoutWidget);
        find_domen_line->setObjectName(QStringLiteral("find_domen_line"));
        find_domen_line->setFont(font1);

        horizontalLayout_3->addWidget(find_domen_line);

        find = new QPushButton(verticalLayoutWidget);
        find->setObjectName(QStringLiteral("find"));
        find->setFont(font1);

        horizontalLayout_3->addWidget(find);

        close = new QPushButton(verticalLayoutWidget);
        close->setObjectName(QStringLiteral("close"));
        close->setFont(font1);

        horizontalLayout_3->addWidget(close);


        verticalLayout->addLayout(horizontalLayout_3);

        listWidget_2 = new QListWidget(verticalLayoutWidget);
        listWidget_2->setObjectName(QStringLiteral("listWidget_2"));

        verticalLayout->addWidget(listWidget_2);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font1);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label);

        link_out_2 = new QLabel(verticalLayoutWidget);
        link_out_2->setObjectName(QStringLiteral("link_out_2"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, link_out_2);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_3);

        position_out_2 = new QLabel(verticalLayoutWidget);
        position_out_2->setObjectName(QStringLiteral("position_out_2"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, position_out_2);

        label_5 = new QLabel(verticalLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font1);

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_5);

        visits_out_2 = new QLabel(verticalLayoutWidget);
        visits_out_2->setObjectName(QStringLiteral("visits_out_2"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, visits_out_2);

        label_7 = new QLabel(verticalLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font1);

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_7);

        author_out_2 = new QLabel(verticalLayoutWidget);
        author_out_2->setObjectName(QStringLiteral("author_out_2"));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, author_out_2);


        verticalLayout->addLayout(formLayout_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 577, 38));
        menuWebsites_Editor = new QMenu(menuBar);
        menuWebsites_Editor->setObjectName(QStringLiteral("menuWebsites_Editor"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setToolTipDuration(10);
        statusBar->setInputMethodHints(Qt::ImhHiddenText|Qt::ImhPreferLowercase);
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuWebsites_Editor->menuAction());
        menuWebsites_Editor->addSeparator();
        menuWebsites_Editor->addAction(load_2);
        menuWebsites_Editor->addAction(save);
        menuWebsites_Editor->addSeparator();
        menuWebsites_Editor->addSeparator();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Website Editor", 0));
        save->setText(QApplication::translate("MainWindow", "save", 0));
        load_2->setText(QApplication::translate("MainWindow", "load", 0));
        add->setText(QApplication::translate("MainWindow", "add", 0));
        load->setText(QApplication::translate("MainWindow", "Load", 0));
        execute->setText(QApplication::translate("MainWindow", "Execute", 0));
        remove->setText(QApplication::translate("MainWindow", "Remove", 0));
        edit->setText(QApplication::translate("MainWindow", "Edit", 0));
        link_in->setText(QApplication::translate("MainWindow", "Link", 0));
#ifndef QT_NO_STATUSTIP
        link_out->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        link_out->setText(QString());
        position_in->setText(QApplication::translate("MainWindow", "position        ", 0));
        position_out->setText(QString());
        visits_in->setText(QApplication::translate("MainWindow", "visits", 0));
        author->setText(QApplication::translate("MainWindow", "Author", 0));
        author_out->setText(QString());
        visits_out->setText(QString());
        find_domen_line->setText(QApplication::translate("MainWindow", ".com", 0));
        find->setText(QApplication::translate("MainWindow", "find", 0));
        close->setText(QApplication::translate("MainWindow", "exit", 0));
        label->setText(QApplication::translate("MainWindow", "Link", 0));
        link_out_2->setText(QString());
        label_3->setText(QApplication::translate("MainWindow", "position", 0));
        position_out_2->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "visits", 0));
        visits_out_2->setText(QString());
        label_7->setText(QApplication::translate("MainWindow", "Author", 0));
        author_out_2->setText(QString());
        menuWebsites_Editor->setTitle(QApplication::translate("MainWindow", "save & load", 0));
#ifndef QT_NO_STATUSTIP
        statusBar->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        statusBar->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

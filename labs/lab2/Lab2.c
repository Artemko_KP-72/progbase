#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
gcc Lab2.c -std=c11 -Wall -Werror -pedantic-errors -lm -lprogbase
*/
// char key = Console_getChar();

void menu(void);
void FirstPart(void);
void FirstPartMenu(int N);
void help(int COLUMN);
void help2(int COLUMN);
void ClearRightPart(int COLUMN);
void SecondPart(void);
void ThirdPart(void);
void titles(void);
void mainhelp(void);
void cleanBuffer() {
  char c = 0;
  while (c != '\n') {
    c = getchar();
  }
}

int main(void) { menu(); }

//                                      menu

void menu(void) {
  Console_hideCursor();
  int switcher = 1;
  char key = '\0';

  while (key != 'E') {

    Console_clear();

    puts("Arrays and strings\n");

    if (switcher == 1)
      Console_setCursorAttribute(FG_YELLOW);

    puts("One-dimensional array ");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 2)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Two-dimensional array");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 3)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Strings");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 4)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Titles");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 5)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Help");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 6)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Exit");

    Console_setCursorAttribute(FG_DEFAULT);

    key = Console_getChar();

    if (key == 10) {

      if (switcher == 1)
        FirstPart();

      if (switcher == 2)
        SecondPart();

      if (switcher == 3)
        ThirdPart();

      if (switcher == 4)
        titles();

      if (switcher == 5)
        mainhelp();

      if (switcher == 6) {
        key = 'E';
        Console_clear();
      }
    }

    if (key == 66)
      switcher++;

    if (key == 65)
      switcher--;

    if (switcher == 0)
      switcher = 6;

    if (switcher == 7)
      switcher = 1;
  }
}

//                                      void FirstPart(void);
void FirstPart(void) {
  int N = 0;

  Console_clear();

  Console_setCursorAttribute(FG_GREEN);

  puts("Please enter N value( N <= 20 )");

  Console_setCursorAttribute(FG_DEFAULT);

  Console_setCursorPosition(5, 0);

  puts("*press any letter to go back");

  Console_setCursorPosition(2, 0);

  // cleanBuffer();

  if (!scanf("%i", &N) || N <= 0 || N > 20) {
    cleanBuffer();
    return;
  }

  FirstPartMenu(N);
}

//                                        FirstTaskMenu

void FirstPartMenu(int N) {

  int L = 0;
  int H = 0;
  int k = 0;
  int asd = 0;
  int mass[N];
  int min = 0;
  int imin = 0;
  int max = 0;
  int imax = 0;
  int sum = 0;
  int product = 1;
  int switcher = 1;
  int cheaker = 0;
  int const COLUMN = 80;
  char key = '\0';

  for (int i = 0; i < N; i++) {
    mass[i] = 0;
  }
  for (int i = 0; i < 15; i++) {
    Console_setCursorPosition(i, COLUMN - 2);
    printf("|");
  }

  while (key != 'E') {

    Console_setCursorPosition(1, 0);

    Console_setCursorAttribute(FG_GREEN);

    puts("One-dimensional array");
    puts("                      ");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 1)
      Console_setCursorAttribute(FG_YELLOW);

    puts("Fill an array with random numbers from L to H.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 2)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Reset all elements of the array.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 3)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Find the minimal element of the array and its index.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 4)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Find the sum of the array elements.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 5)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Output the product of the negative array elements.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 6)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Swap the values of the maximum and minimum elements of the array.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 7)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Multiply all array elements by the entered number.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 8)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Clear Right Pannel.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 9)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Help.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 10)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Exit.");

    Console_setCursorAttribute(FG_DEFAULT);

    key = Console_getChar();

    if (key == 10) {

      if (switcher == 1) {

        ClearRightPart(COLUMN);
        Console_setCursorPosition(1, COLUMN);
        printf("H value is ");

        if (!scanf("%i", &H)) {
          cleanBuffer();
          printf("Please enter valid value next time.");
          return;
        }

        Console_setCursorPosition(2, COLUMN);
        printf("L value is ");

        if (!scanf("%i", &L)) {
          cleanBuffer();
          printf("Please enter valid value next time.");
          return;
        }
        ClearRightPart(COLUMN);

        if (H >= L)
          asd = H;
        else {
          asd = L;
          L = H;
          H = asd;
        }
        Console_setCursorPosition(1, COLUMN);
        for (int i = 0; i < N; i++) {
          mass[i] = (rand() % (H - L + 1)) + L;
          printf(" %2i ", mass[i]);
        }
      }

      if (switcher == 2) {
        ClearRightPart(COLUMN);
        Console_setCursorPosition(1, COLUMN);
        for (int i = 0; i < N; i++) {
          mass[i] = 0;
          printf(" %2i ", mass[i]);
        };
      }
      if (switcher == 3) {

        ClearRightPart(COLUMN);

        min = mass[0];
        imin = 0;

        for (int i = 1; i < N; i++) {
          if (min > mass[i]) {
            min = mass[i];
            imin = i;
          }
        }

        Console_setCursorPosition(1, COLUMN);

        for (int i = 0; i < N; i++) {
          if (imin == i) {
            Console_setCursorAttribute(BG_BLUE);
          }
          printf(" %2i ", mass[i]);
          Console_setCursorAttribute(BG_DEFAULT);
        }
      }

      if (switcher == 4) {

        ClearRightPart(COLUMN);

        sum = 0;
        Console_setCursorPosition(3, COLUMN);
        for (int i = 0; i < N; i++) {
          sum += mass[i];
        }
        printf("  sum is %3i", sum);
      }

      if (switcher == 5) {
        ClearRightPart(COLUMN);
        product = 1;
        cheaker = 0;
        Console_setCursorPosition(4, COLUMN);
        for (int i = 0; i < N; i++) {
          if (mass[i] < 0) {
            product *= mass[i];
            cheaker = 1;
          }
        }
        if (cheaker == 0)
          printf("  Array composed only from positive numbers");
        else
          printf("  product is %3i", product);
      }

      if (switcher == 6) {
        ClearRightPart(COLUMN);
        imax = 0;
        max = mass[0];
        min = mass[0];
        imin = 0;
        for (int i = 1; i < N; i++) {
          if (min > mass[i]) {
            min = mass[i];
            imin = i;
          }
          if (max <= mass[i]) {
            max = mass[i];
            imax = i;
          }
        }
        asd = mass[imin];
        mass[imin] = mass[imax];
        mass[imax] = asd;
        Console_setCursorPosition(1, COLUMN);
        for (int i = 0; i < N; i++) {
          if (i == imin || i == imax) {
            Console_setCursorAttribute(BG_BLUE);
          }
          printf(" %2i ", mass[i]);
          Console_setCursorAttribute(BG_DEFAULT);
        }
      }
      if (switcher == 7) {
        ClearRightPart(COLUMN);
        Console_setCursorPosition(5, COLUMN);
        printf("                                      ");
        Console_setCursorPosition(5, COLUMN);
        printf("Please enter multiplyer ");

        if (!scanf("%i", &k)) {
          Console_setCursorPosition(5, COLUMN);
          cleanBuffer();
          printf("Please enter valid value next time");
        }
        Console_setCursorPosition(1, COLUMN);
        for (int i = 0; i < N; i++) {
          mass[i] *= k;
          printf(" %2i ", mass[i]);
        }
      }

      if (switcher == 8)
        ClearRightPart(COLUMN);

      if (switcher == 9)
        help(COLUMN);

      if (switcher == 10)
        key = 'E';
    }

    if (key == 66)
      switcher++;

    if (key == 65)
      switcher--;

    if (switcher == 0)
      switcher = 10;

    if (switcher == 11)
      switcher = 1;
  }
}

//                                      help

void help(int COLUMN) {
  Console_setCursorPosition(1, COLUMN);
  printf("У цьому стані у користувача є одномірний масив цілих чисел (розміром "
         "N елементів) ініціалізований нулями. ");
  Console_setCursorPosition(2, COLUMN);
  printf("      Доступні операції над масивом:");
  Console_setCursorPosition(3, COLUMN);
  printf(" Заповнити масив випадковими числами від L до H.");
  Console_setCursorPosition(4, COLUMN);
  printf(" Обнулити всі елементи масиву.");
  Console_setCursorPosition(5, COLUMN);
  printf(" Знайти мінімальний елемент масиву та його індекс.");
  Console_setCursorPosition(6, COLUMN);
  printf(" Знайти суму елементів масиву.");
  Console_setCursorPosition(7, COLUMN);
  printf(" Вивести добуток від'ємних елементів масиву.");
  Console_setCursorPosition(8, COLUMN);
  printf(" Поміняти місцями значення максимального і мінімального елементів "
         "масиву.");
  Console_setCursorPosition(9, COLUMN);
  printf(" Помножити всі елементи масиву на введене число.  ");
}

//                                      ClearRightPart

void ClearRightPart(int COLUMN) {
  for (int i = 0; i < 21; i++) {
    Console_setCursorPosition(i, COLUMN);
    printf("                                                                   "
           "                               ");
  }
  Console_setCursorPosition(1, COLUMN);
}

//                                        Second part
void SecondPart(void) {
  int N = 0;
  int switcher = 1;
  char key = '\0';
  int asd = 0;
  int H = 0;
  int L = 0;
  int colsum = 0;
  int sum = 0;

  int imin_i = 0;
  int imin_j = 0;
  int min = 0;
  int imax_i = 0;
  int imax_j = 0;
  int max = 0;

  int const COLUMN = 80;

  Console_clear();

  Console_setCursorAttribute(FG_GREEN);

  puts("Please enter N value (N <= 8 )");

  Console_setCursorAttribute(FG_DEFAULT);

  Console_setCursorPosition(5, 0);

  puts("*press any letter to go back");

  Console_setCursorPosition(2, 0);

  // cleanBuffer();

  if (!scanf("%i", &N) || N <= 0 || N > 8) {
    cleanBuffer();
    return;
  }

  int mass[N][N];

  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      mass[i][j] = 0;
    }
  }

  for (int i = 0; i < 15; i++) {
    Console_setCursorPosition(i, COLUMN - 2);
    printf("|");
  }

  //                                     //while

  while (key != 'E') {

    Console_setCursorPosition(1, 0);

    Console_setCursorAttribute(FG_GREEN);

    puts("Two-dimensional array");
    puts("                      ");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 1)
      Console_setCursorAttribute(FG_YELLOW);

    puts("Fill an array with random numbers from L to H.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 2)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Reset all elements of the array.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 3)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Find the maximal element of the array and its index.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 4)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Find the number of elements in the side array of the array.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 5)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Find the sum of column items by a given index.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 6)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Swap places the maximum and minimum elements of the array.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 7)
      Console_setCursorAttribute(FG_YELLOW);
    puts(
        "Change the value of an item by specified indexes to a specified one.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 8)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Clear Right Pannel.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 9)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Help.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 10)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Exit.");

    Console_setCursorAttribute(FG_DEFAULT);

    //                              Menu

    key = Console_getChar();

    if (key == 10) {

      //        switch1

      if (switcher == 1) {

        ClearRightPart(COLUMN);

        Console_setCursorPosition(1, COLUMN);
        printf("H value is ");

        if (!scanf("%i", &H)) {
          cleanBuffer();
          printf("Please enter valid value next time.");
          return;
        }

        Console_setCursorPosition(2, COLUMN);
        printf("L value is ");

        if (!scanf("%i", &L)) {
          cleanBuffer();
          printf("Please enter valid value next time.");
          return;
        }
        ClearRightPart(COLUMN);

        if (H >= L)
          asd = H;
        else {
          asd = L;
          L = H;
          H = asd;
        }

        for (int i = 0; i < N; i++) {
          Console_setCursorPosition((2 * (i)) + 1, COLUMN);
          for (int j = 0; j < N; j++) {
            mass[i][j] = (rand() % (H - L + 1)) + L;
            printf(" %2i ", mass[i][j]);
          }
        }
      }

      //  switcher2

      if (switcher == 2) {

        ClearRightPart(COLUMN);

        for (int i = 0; i < N; i++) {
          Console_setCursorPosition((2 * (i)) + 1, COLUMN);
          for (int j = 0; j < N; j++) {
            mass[i][j] = 0;
            printf(" %2i ", mass[i][j]);
          }
        }
      }

      //  switcher3

      if (switcher == 3) {

        ClearRightPart(COLUMN);

        imax_i = 0;
        imax_j = 0;
        max = 0;
        for (int i = 0; i < N; i++) {
          for (int j = 0; j < N; j++) {
            if (max <= mass[i][j]) {
              max = mass[i][j];
              imax_i = i;
              imax_j = j;
            }
          }
        }

        //             output array

        for (int i = 0; i < N; i++) {
          Console_setCursorPosition((2 * (i)) + 1, COLUMN);
          for (int j = 0; j < N; j++) {

            if (i == imax_i && j == imax_j)
              Console_setCursorAttribute(BG_INTENSITY_RED);

            printf(" %2i ", mass[i][j]);
            Console_setCursorAttribute(BG_DEFAULT);
          }
        }
      }

      //   switch4

      if (switcher == 4) {

        ClearRightPart(COLUMN);

        for (int i = 0; i < N; i++) {
          Console_setCursorPosition((2 * (i)) + 1, COLUMN);
          for (int j = 0; j < N; j++) {

            if (j + i == N - 1)
              Console_setCursorAttribute(BG_INTENSITY_RED);

            printf(" %2i ", mass[i][j]);
            Console_setCursorAttribute(BG_DEFAULT);
          }
        }
      }

      //   switch5

      if (switcher == 5) {
        char switch5 = '\0';
        colsum = 0;
        sum = 0;

        ClearRightPart(COLUMN);

        Console_setCursorPosition(1, COLUMN);

        printf("Enter Please a number of column ");

        Console_setCursorPosition(2, COLUMN);

        printf("*A number shoud be valid ");

        switch5 = '\0';

        while (switch5 != '@') {
          if (!scanf("%i", &colsum) || (colsum <= 0 || colsum > N)) {
            cleanBuffer();
            ClearRightPart(COLUMN);
            Console_setCursorPosition(1, COLUMN);
            printf("Enter Please a number of column ");
            Console_setCursorPosition(2, COLUMN);
            printf("*A NUMBER SHOUD BE VALID!!!!");
            continue;
          } else
            switch5 = '@';
        }

        for (int i = 0; i < N; i++) {
          sum += mass[i][colsum - 1];
        }

        ClearRightPart(COLUMN);

        Console_setCursorPosition(1, COLUMN);

        printf("sum in %i column is %i                ", colsum, sum);
      }

      //  switch6

      if (switcher == 6) {

        ClearRightPart(COLUMN);

        imin_i = 0;
        imin_j = 0;
        imax_i = 0;
        imax_j = 0;
        min = mass[0][0];
        max = mass[N - 1][N - 1];

        for (int i = 0; i < N; i++) {
          for (int j = 0; j < N; j++) {
            if (min > mass[i][j]) {
              min = mass[i][j];
              imin_i = i;
              imin_j = j;
            }
            if (max <= mass[i][j]) {
              max = mass[i][j];
              imax_i = i;
              imax_j = j;
            }
          }
        }

        mass[imin_i][imin_j] = max;
        mass[imax_i][imax_j] = min;

        for (int i = 0; i < N; i++) {
          Console_setCursorPosition((2 * (i)) + 1, COLUMN);
          for (int j = 0; j < N; j++) {

            if (i == imax_i && j == imax_j) {
              Console_setCursorAttribute(BG_BLUE);
              printf("%2i ", min);
              Console_setCursorAttribute(BG_DEFAULT);
            } else {
              if (i == imin_i && j == imin_j) {
                Console_setCursorAttribute(BG_RED);
                printf(" %2i ", max);
                Console_setCursorAttribute(BG_DEFAULT);
              } else {
                printf(" %2i ", mass[i][j]);
              }
            }
          }
        }
      }

      //      switch7

      if (switcher == 7) {
        int i = 0;
        int j = 0;

        ClearRightPart(COLUMN);
        Console_setCursorPosition(1, COLUMN);
        printf("Enter the i and j vallue (valid please, i,j < N)");
        Console_setCursorPosition(2, COLUMN);
        printf("i = ");
        char switch5 = '\0';
        //                                    i
        while (switch5 != '@') {
          if (!scanf("%i", &i) || (i <= 0 || i > N)) {
            cleanBuffer();
            ClearRightPart(COLUMN);
            Console_setCursorPosition(1, COLUMN);
            printf("Enter the i and j vallue (valid please, i,j < N)");
            Console_setCursorPosition(2, COLUMN);
            printf("i = ");
            continue;
          } else {
            if (i > 0 && i <= N)
              switch5 = '@';
          }
        }
        switch5 = '\0';
        //                  j

        ClearRightPart(COLUMN);
        Console_setCursorPosition(1, COLUMN);
        printf("Enter the i and j vallue (valid please, i,j < N)");
        Console_setCursorPosition(2, COLUMN);
        printf("j = ");
        switch5 = '\0';

        while (switch5 != '@') {
          if (!scanf("%i", &j) || (j <= 0 || j > N)) {
            cleanBuffer();
            ClearRightPart(COLUMN);
            Console_setCursorPosition(1, COLUMN);
            printf("Enter the i and j vallue (valid please, i,j < N)");
            Console_setCursorPosition(2, COLUMN);
            printf("j =");
            continue;
          } else {
            if (j > 0 && j <= N)
              switch5 = '@';
            else {
              ClearRightPart(COLUMN);
              cleanBuffer();
            }
          }
        }

        //              new a[i][j]

        int tmp = 0;

        ClearRightPart(COLUMN);
        Console_setCursorPosition(1, COLUMN);
        printf("Enter a new array value");
        Console_setCursorPosition(2, COLUMN);
        printf("mass[%i][%i] = ", i, j);
        switch5 = '\0';

        while (switch5 != '@') {
          if (!scanf("%i", &tmp)) {
            cleanBuffer();
            ClearRightPart(COLUMN);
            Console_setCursorPosition(1, COLUMN);
            printf("Enter a new array value");
            Console_setCursorPosition(2, COLUMN);
            printf("mass[%i][%i] = ", i, j);
            continue;
          } else
            switch5 = '@';
        }

        mass[i - 1][j - 1] = tmp;
        ClearRightPart(COLUMN);

        for (int i = 0; i < N; i++) {
          Console_setCursorPosition(2 * i + 1, COLUMN);
          for (int j = 0; j < N; j++) {
            printf(" %2i ", mass[i][j]);
          }
        }
      }

      // switch 8
      if (switcher == 8)
        ClearRightPart(COLUMN);

      //             switch 9
      if (switcher == 9)
        help2(COLUMN);

      if (switcher == 10)
        key = 'E';
    }

    //                            Switcher Control
    if (key == 66)
      switcher++;

    if (key == 65)
      switcher--;

    if (switcher == 0)
      switcher = 10;

    if (switcher == 11)
      switcher = 1;

    //                            Switcher Control
  }
}

void help2(int COLUMN) {
  Console_setCursorPosition(1, COLUMN);
  printf("У цьому стані у користувача є двомірний масив цілих чисел "
         "(квардратна матриця NхN) ініціалізований нулями.");
  Console_setCursorPosition(3, COLUMN);
  printf("      Доступні операції над матрицею:");
  Console_setCursorPosition(4, COLUMN);
  printf("Заповнити масив випадковими числами від L до H.");
  Console_setCursorPosition(5, COLUMN);
  printf("Обнулити всі елементи масиву.");
  Console_setCursorPosition(6, COLUMN);
  printf("Знайти максимальний елемент та його індекси (i та j).");
  Console_setCursorPosition(7, COLUMN);
  printf("Знайти суму елементів побічної діагоналі масиву.");
  Console_setCursorPosition(8, COLUMN);
  printf("Знайти суму елементів стовпця за заданим індексом.");
  Console_setCursorPosition(9, COLUMN);
  printf("Поміняти місцями максимальний і мінімальний елементи масиву.");
  Console_setCursorPosition(10, COLUMN);
  printf("Змінити значення елементу за вказаними індексами на задане.");
}

//                                        //Third part
void ThirdPart(void) {
  const int COLUMN = 80;
  Console_clear();
  int switcher = 1;
  char key = '\0';
  int N = 0;
  int j = 3;
  srand(time(NULL));
  puts("Enter N value, please(N > 0, and N <= 80");

  if (!scanf("%i", &N) || N < 0 || N > 80)
    return;

  char str[N + 1];
  Console_setCursorPosition(1, COLUMN);
  for (int i = 0; i < N; i++) {
    str[i] = rand() % (126 - 32 + 1) + 32;
    printf("%c", str[i]);
  }

  str[N] = '\0';
  while (key != 'E') {

    Console_setCursorPosition(1, 0);

    Console_setCursorAttribute(FG_GREEN);

    puts("Handling rows                                                   ");
    puts("                                                                     "
         "        ");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 1)
      Console_setCursorAttribute(FG_YELLOW);

    puts("Fill in the entered value from the console (gaps should also work).  "
         "        ");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 2)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Clear the line.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 3)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Output a substrate from a given position and a given length.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 4)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Output a list of substrings separated by a given character.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 5)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Output the shortest word.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 6)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Find and print all the fractional numbers contained in the string.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 7)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Find and output the product of all integers contained in a string.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 8)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Clear Right Pannel.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 9)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Help.");

    Console_setCursorAttribute(FG_DEFAULT);

    if (switcher == 10)
      Console_setCursorAttribute(FG_YELLOW);
    puts("Exit.");

    Console_setCursorAttribute(FG_DEFAULT);

    for (int i = 0; i < 15; i++) {
      Console_setCursorPosition(i, COLUMN - 2);
      printf("|");
    }
    Console_setCursorPosition(j + 1, COLUMN);

    //                              Menu

    key = Console_getChar();

    if (key == 10) {
      if (switcher == 1) {
        ClearRightPart(COLUMN);
        Console_setCursorPosition(1, COLUMN);
        for (int i = 0; i < N; i++) {
          printf("%c", str[i]);
        }

        Console_setCursorPosition(2, COLUMN);
        printf("Enter your string ");
        char unvis[201];
        // cleanBuffer();
        Console_setCursorPosition(1, COLUMN);

        fgets(unvis, N + 1, stdin);
        strncpy(str, unvis, N);

        Console_setCursorPosition(1, COLUMN);
        for (int i = 0; i < N + 1; i++) {
          printf("%c", str[i]);
        }
      }
      if (switcher == 2) {
        ClearRightPart(COLUMN);
        Console_setCursorPosition(1, COLUMN);
        for (int i = 0; i < N; i++) {
          str[i] = ' ';
          printf("%c", str[i]);
        }
      }

      if (switcher == 3) {
        cleanBuffer();
        int tmp = 0;
        int length = 0;
        char keyer = '0';

        while (keyer != '\0') {
          Console_setCursorPosition(1, COLUMN);
          ClearRightPart(COLUMN);
          printf("please, enter index of first element ");
          if (!scanf("%i", &tmp) || tmp < 0 || tmp > N + 1) {
            cleanBuffer();
            continue;
          } else
            keyer = '\0';
        }

        keyer = '0';

        while (keyer != '\0') {
          Console_setCursorPosition(1, COLUMN);
          ClearRightPart(COLUMN);
          printf("please, enter desirable length ");
          if (!scanf("%i", &length) || length < 0 || length + tmp > N + 1) {
            cleanBuffer();
            continue;
          } else
            keyer = '\0';
        }

        Console_setCursorPosition(1, COLUMN);
        ClearRightPart(COLUMN);

        for (int i = tmp - 1; i < tmp + length; i++) {
          printf("%c", str[i]);
        }
      }
      //                          switcher4
      if (switcher == 4) {
        ClearRightPart(COLUMN);
        char desi = '\0';
        char yo = '\0';
        j = 2;
        Console_setCursorPosition(1, COLUMN);
        printf("Enter the desirable symbol ");

        yo = '\0';
        scanf("%c", &desi);
        while (yo != '@') {
          if (!scanf("%c", &desi))
            continue;
          else
            yo = '@';
        }

        Console_setCursorPosition(2, COLUMN);

        for (int i = 0; i < N; i++) {
          if (str[i] == desi) {
            j++;
            Console_setCursorPosition(j, COLUMN);
          }
          printf("%c", str[i]);
        }
      }
      //                    switcher5
      if (switcher == 5) {
        char begin = -1;
        char end = -1;
        int wordpoint = 1;
        char beginmin = -1;
        char endmin = -1;
        char minLength = strlen(str);
        for (int i = 0; i < strlen(str); i++) {

          if (isalpha(str[i]) && (i == 0 || !isalpha(str[i - 1]))) {
            begin = i;
            wordpoint = 1;
          }
          if (isalpha(str[i]) &&
              (i == strlen(str) - 1 || !isalpha(str[i + 1]))) {
            end = i;
            wordpoint = 0;
          }
          if (!wordpoint && end - begin + 1 < minLength) {
            beginmin = begin;
            endmin = end;
            minLength = end - begin + 1;
          }
        }
        Console_setCursorPosition(2, COLUMN);
        if (minLength != 0) {
          printf("Shortest word is ");
          for (int i = beginmin; i <= endmin; i++) {
            printf("%c", str[i]);
          }
        } else {
          printf("String don`t heve words ");
        }
      }
      if (switcher == 6) {
        Console_setCursorPosition(3, COLUMN);
        for (int i = 1; i < N; i++) {
          if (str[i] == '.' && isdigit(str[i + 1]) && isdigit(str[i - 1])) {
            //                    begining of huge code
            int left = i - 1;
            int right = i + 1;
            while (isdigit(str[left - 1])) {
              left--;
            }
            while (isdigit(str[right + 1])) {
              right++;
            }

            if (str[left - 1] == '-' && left > 0) {
              printf("-");
            }
            Console_setCursorPosition(i + 3, COLUMN);
            for (int k = left; k <= right; k++) {
              printf("%c", str[k]);
            }
            printf(" ");
            //                    ending of huge code
          }
          Console_setCursorPosition(4, COLUMN);
        }
      }
      if (switcher == 7) {
        long product = 1;
        for (int i = 0; i < strlen(str); i++) {
          if (isdigit(str[i])) {
            product *= (str[i] - '0');
          }
        }
        Console_setCursorPosition(2, COLUMN);
        printf("The product is %ld", product);
      }
      if (switcher == 8) {
        ClearRightPart(COLUMN);
      }
      if (switcher == 9) {
        Console_setCursorPosition(1, COLUMN);
        printf("Обробка рядків мови С.");
        Console_setCursorPosition(3, COLUMN);
        printf("      Доступні операції над рядком:");
        Console_setCursorPosition(4, COLUMN);
        printf("Заповнити строку введеним значенням із консолі (пробіли також "
               "мають працювати).");
        Console_setCursorPosition(5, COLUMN);
        printf("Очистити строку.");
        Console_setCursorPosition(6, COLUMN);
        printf("Вивести підстроку із заданої позиції і заданої довжини.");
        Console_setCursorPosition(7, COLUMN);
        printf("Вивести список підстрок, розділених заданим символом.");
        Console_setCursorPosition(8, COLUMN);
        printf("Вивести найкоротше слово (слова - непуста послідовність "
               "буквенних символів).");
        Console_setCursorPosition(9, COLUMN);
        printf("Знайти та вивести всі дробові числа, що містяться у строці.");
        Console_setCursorPosition(10, COLUMN);
        printf("Знайти та вивести добуток всіх цілих чисел, що містяться у "
               "строці.");
      }

      if (switcher == 10) {
        key = 'E';
      }
    }

    //                    walkers

    if (key == 66)
      switcher++;

    if (key == 65)
      switcher--;

    if (switcher == 0)
      switcher = 10;

    if (switcher == 11)
      switcher = 1;

    //        switch1
  }
}
void titles(void) {
  struct ConsoleSize console = Console_size();
  Console_hideCursor();
  for (int i = 1; i < console.rows + 18; i++) {
    Console_clear();
    Console_setCursorPosition(i - 18, 50);
    if (i > 18 && i < console.rows + 18)
      printf("                       Glory to Ukraine ");
    Console_setCursorPosition(i - 16, 50);
    if (i > 16 && i < console.rows + 16)
      printf("        Without them I would not have done laboratory work");
    Console_setCursorPosition(i - 14, 50);
    if (i > 14 && i < console.rows + 14)
      printf("          my computer, my cat, my VScode and Dennis Ritchi");
    Console_setCursorPosition(i - 12, 50);
    if (i > 12 && i < console.rows + 12)
      printf("                 Olya, Nastya, Nadya, Amir, Oleg");
    Console_setCursorPosition(i - 10, 50);
    if (i > 10 && i < console.rows + 10)
      printf("                Artem, Nikita, Misha, Yaroslav, Vitya, Sasha");
    Console_setCursorPosition(i - 8, 50);
    if (i > 8 && i < console.rows + 8)
      printf("                     Dima, Dan, Bogdan, Igor");
    Console_setCursorPosition(i - 6, 50);
    if (i > 6 && i < console.rows + 6)
      printf(
          "  Phadynyak Ruslan Anatoliyevych - the author of idea this project");
    Console_setCursorPosition(i - 4, 50);
    if (i > 4 && i < console.rows + 4)
      printf("               I want to express my gratitude to:");
    Console_setCursorPosition(i - 2, 50);
    if (i > 2 && i < console.rows + 2)
      puts("              I hope you enjoyed using my creature");
    Console_setCursorPosition(i, 50);
    if (i < console.rows)
      puts("                     This is my second lab");
    sleepMillis(850);
  }
}
void mainhelp(void) {
  int const COLUMN = 80;
  char wait = '\0';
  Console_setCursorPosition(1, COLUMN);
  printf("Робота з одномірними та багатомірними масивами. Рядки. ");
  Console_setCursorPosition(3, COLUMN);
  printf("      Доступні завдання:");
  Console_setCursorPosition(4, COLUMN);
  printf("Завдання №1. Одномірний масив");
  Console_setCursorPosition(5, COLUMN);
  printf("Завдання №2. Двомірний масив");
  Console_setCursorPosition(6, COLUMN);
  printf("Завдання №3. Обробка рядків мови С.");
  Console_setCursorPosition(7, COLUMN);
  printf("Перегляд титрів");
  Console_setCursorPosition(8, COLUMN);
  printf("Переглянути допомогу");
  Console_setCursorPosition(9, COLUMN);
  printf("Вихід із програми \"E\" або \"Exit\"");
  Console_setCursorPosition(10, COLUMN);
  printf("Натисніть будь яку клавішу для виходу");
  wait = Console_getChar();
  if (wait == 'y') {
    wait = 't';
  }
}
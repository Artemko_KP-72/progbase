#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_LENGTH 100

int menu(char *names, int names_count);
int main(void) { menu("First#Second", 2); }
int menu(char *names, int names_count) {
  char *names2[names_count][BUFFER_LENGTH];
  int nread = 0;
  int step = 0;
  for (int i = 0; i < names_count; i++) {
    step += sscanf( names + step, "%[^#]%*c %n", *names2[i], &nread);
  }
  Console_hideCursor();
  int switcher = 1;
  char key = '\0';

  while (key != 'E') {

    Console_clear();

    puts("Arrays and strings\n");
    for (int i = 0; i < names_count; i++) {
      if (switcher == i + 1)
        Console_setCursorAttribute(FG_YELLOW);
        printf("%s", &names[i]);
    
        Console_setCursorAttribute(FG_DEFAULT);
    }
    key = Console_getChar();
    
        if (key == 10) {
    
          if (switcher == 1)
    
          if (switcher == 2)
    
          if (switcher == 3)
    
          if (switcher == 4)
    
          if (switcher == 5)
    
          if (switcher == 6) {
            key = 'E';
            Console_clear();
          }
        }
    
        if (key == 66)
          switcher++;
    
        if (key == 65)
          switcher--;
    
        if (switcher == 0)
          switcher = 6;
    
        if (switcher == 7)
          switcher = 1;
    }
    return 1;
}
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define O 2
#define PI 3.14

struct connect {
  int x;
  int y;
};

void segment(int x0, int y0, int x1, int y1);

int main(void) {
  struct connect center = {.x = 21, .y = 32};
  int const r = 16;
  struct connect circle = {.x = center.x - r*cos(PI/2), .y = O * (center.y + r*sin(PI/2))};
  int r2 = pow(r, 2);
  system("clear");

  Console_setCursorPosition(center.x, center.y);

  Console_setCursorAttribute(BG_RED);

  struct ConsoleSize console = Console_size();
  for (int i = 0; i < console.rows; i++) {
    for (int j = 0; j < console.columns; j++) {
      Console_setCursorPosition(i, O * j);
      if ((pow(i - center.x, 2) + pow(j - center.y, 2) - r2) <= 1)
        printf("  ");
    }
  }
  Console_setCursorAttribute(BG_CYAN);

  for (int i = 0; i < console.rows; i++) {
    for (int j = 0; j < console.columns; j++) {
      Console_setCursorPosition(i, O * j);
      if (pow(i - center.x, 2) + pow(j - center.y, 2) - (r2 - 2 * r) <= 1)
        printf("  ");
    }
  }

  Console_setCursorAttribute(BG_GREEN);
  Console_setCursorPosition(21, O * 32);
  printf("+");

  Console_setCursorAttribute(BG_YELLOW);
  segment(circle.x, circle.y, center.x, O * center.y);
  circle.x = center.x  - r*cos(0*PI/4);
  printf("%i", circle.x);
  circle.y = center.y + r*sin( 0*PI/4);
  segment(circle.x, circle.y, center.x, O * center.y);
  int i = 5;
  while (1) {
    segment(circle.x, circle.y, center.x, O * center.y);
    sleepMillis(500);
    circle.x = center.x  - r*cos(i * PI/4);
    printf("%i", circle.x);
    circle.y = center.y + r*sin(i * PI/4);
    i++;
    if (i == 15)
      i = 1;

}
  Console_setCursorPosition(0, 0);

  return 0;
}

//       unknown function

void segment(int x0, int y0, int x1, int y1) {
  int dx = abs(x1 - x0);
  int dy = abs(y1 - y0);
  int sx = x1 >= x0 ? 1 : -1;
  int sy = y1 >= y0 ? 1 : -1;

  Console_setCursorAttribute(BG_BLACK);

  if (dy <= dx) {
    int d = (dy << 1) - dx;
    int d1 = dy << 1;
    int d2 = (dy - dx) << 1;

    Console_setCursorPosition(x0, y0);
    printf("  ");
    for (int x = x0 + sx, y = y0, i = 1; i <= dx; i++, x += sx) {
      if (d > 0) {
        d += d2;
        y += sy;
      } else
        d += d1;
      Console_setCursorPosition(x, y);
      printf("  ");
    }
  } else {
    int d = (dx << 1) - dy;
    int d1 = dx << 1;
    int d2 = (dx - dy) << 1;
    Console_setCursorPosition(x0, y0);
    printf("  ");
    for (int y = y0 + sy, x = x0, i = 1; i <= dy; i++, y += sy) {
      if (d > 0) {
        d += d2;
        x += sx;
      } else
        d += d1;
      Console_setCursorPosition(x, y);
      printf("  ");
    }
  }
}
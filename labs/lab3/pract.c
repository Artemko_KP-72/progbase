#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

struct Student {
    char name[100];
    time_t birthdate;
};

/*struct tm {
    int tm_year;
    int tm_mon;
    int tm_mday;
    int tm_hour;
    int tm_sec;
    int tm_min;
};*/

time_t createDate(int day, int month, int year);
int getAge(time_t t1, time_t t2);
void printStudent(struct Student * student);
void printStudents(int len, struct Student students[]);
int getStudentsAgeGte(int len, struct Student out[], struct Student in[], int age);

int main(void) { 
    struct Student students[] = {
        {
            .name = "Ira X",
            .birthdate = createDate(9, 4, 2000)
        },
        // @todo more students
    };
    printf("%li", students[0].birthdate);
    //
    return 0;
}

/*time_t createDate(int day, int month, int year){
    time_t data;
    long temp = (year - 1970) / 4;
    data = (year - 1970 - temp) * 24 * 60 * 60 * 365 + temp * 24 * 60 * 366;
    return time_t;
}*/
time_t createDate(int day, int month, int year)
{
    struct tm time;
    time.tm_year = year - 1900;
    time.tm_mon = month;
    time.tm_mday = day;
    time.tm_hour = 0;
    time.tm_sec = 0;
    time.tm_min = 0;
    return mktime(&time);
}
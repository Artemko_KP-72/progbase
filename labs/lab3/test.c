#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

struct Student {
    char name[100];
    time_t birthdate;
};
//
time_t createDate(int day, int month, int year);
int getAge(time_t t1, time_t t2);
int getStudentsAgeGte(int len, struct Student out[], struct Student in[], int age);
//
void printStudent(struct Student * student);
void printStudents(int len, struct Student students[]);
char * formatLocalTime(char * buf, int bufLen, time_t time);

int main(void) { 
    time_t birthdate = createDate(11, 06, 2000);
    time_t now = time(NULL);
    int years = getAge(birthdate, now);
    printf("years: %i\n", years);
    return 0;
}

int getAge(time_t t1, time_t t2) {
    struct tm tm1 = *localtime(&t1);
    struct tm tm2 = *localtime(&t2);
    int years = tm2.tm_year - tm1.tm_year;
    if (tm2.tm_mon < tm1.tm_mon || (tm2.tm_mon == tm1.tm_mon && tm2.tm_mday < tm1.tm_mday)) {
        years -= 1;
    }
    return years;
}

time_t createDate(int day, int month, int year) {
    struct tm t = {0};
    t.tm_mday = day;
    t.tm_mon = month - 1;
    t.tm_year = year - 1900;
    return mktime(&t);
}

char * formatLocalTime(char * buf, int bufLen, time_t time) {
    strftime(buf, bufLen, "%d.%m.%Y", localtime(&time));
    return buf;
}
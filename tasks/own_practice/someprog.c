#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
  struct ConsoleSize console = Console_size();
  int pieceOFSnow[console.rows][console.columns];
  for (int i = 0; i < console.rows; i++) {
    for (int j = 0; j < console.columns; j++) {
      pieceOFSnow[i][j] = -1;
    }
  }
  srand(time(0));
  for (int i = 0; i < console.rows; i++) {
    for (int j = 0; j <= 0.5 * console.columns; j++) {
      int n = rand() % (console.columns + 1);
      pieceOFSnow[i][n] = rand() % 2;
    }
  }
  Console_setCursorAttribute(BG_BLACK);
  int zsuf = 0;
  while (1) {
    Console_clear();
    for (int i = 0; i < console.rows; i++) {
        int row = (i + zsuf) % console.rows + 1;
      for (int j = 0; j < console.columns; j++) {
        char color = FG_GREEN;
        int n = rand() % 2;
        if (n == 0)
        {
            color = FG_INTENSITY_GREEN;
        }
        Console_setCursorAttribute(color);
        Console_setCursorPosition(row, j + 1);
        if (pieceOFSnow[i][j] != -1) {
          printf("%i", pieceOFSnow[i][j]);
        } else {
          printf(" ");
        }
      }
    }
    zsuf++;
    zsuf = zsuf % console.rows;
    sleepMillis(100);
  }
  return EXIT_SUCCESS;
}
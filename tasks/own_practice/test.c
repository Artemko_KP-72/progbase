#include <progbase.h>
#include <progbase/console.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define PERCENT 50
#define SLEEP 50

int main() {
  Console_clear();
  conHideCursor();
  int n = 0;
  int i = 0;
  int j = 0;
  struct ConsoleSize console = Console_size();
  int pieceOFSnow[200][400];
  for (i = 0; i < 200; i++) {
    for (j = 0; j < 400; j++) {
      pieceOFSnow[i][j] = -1;
    }
  }
  srand(time(0));
  Console_setCursorAttribute(FG_GREEN);
  Console_setCursorAttribute(BG_BLACK);

  while (1) {
    console = Console_size();
    for (j = 0; j < console.columns; j++) {
      for (i = console.rows - 1; i > 0; i--) {
        pieceOFSnow[i][j] = pieceOFSnow[i - 1][j];
        Console_setCursorPosition(i + 1, j + 1);
        if (pieceOFSnow[i][j] == 1 || pieceOFSnow[i][j] == 2)
          printf("%i", pieceOFSnow[i][j] - 1);
        else
          printf(" ");
      }
      n = rand() % 100 + 1;
      if (n <= PERCENT)
        pieceOFSnow[0][j] = rand() % 2 + 1;
      else
        pieceOFSnow[0][j] = -1;
    }
    sleepMillis(SLEEP);
  }
  return EXIT_SUCCESS;
}
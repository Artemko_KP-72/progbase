#include <assert.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
gcc nodes.c -std=c11 -Wall -Werror -pedantic-errors -lm -lprogbase
*/

struct nodes {
  int data;
  struct nodes *point;
};

int main(void) {
  struct nodes list[5];
  int index = 0;
  for (int i = 0; i < 5; i++) {
    list[i].data = 97 + i;
    if (i < 4)
      list[i].point = &list[i + 1];
    else
      list[i].point = NULL;
  }
  if (&list[0] != NULL && list[0].point != NULL) {
	
    for (int i = 0; list[i].point != NULL; i++) {
		printf("");
  } 
  }
  printf("Enter index vallue\n 1 <= index <= 5\n");
  scanf("%i", &index);
  for (int i = index; i <= 5; i++) {
    printf("%i ", list[i - 1].data);
  }
}
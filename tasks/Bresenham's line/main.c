#include <assert.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
gcc main.c -std=c11 -Wall -Werror -pedantic-errors -lm -lprogbase
*/

struct point {
  float x;
  float y;
};

int main(void) {

  float tg = 0;
  struct point a;
  struct point b;

  Console_clear();
  srand(time(NULL));

  printf("X1 = ");
  scanf("%f", &a.x);
  // a.x*=2;
  printf("Y1 = ");
  scanf("%f", &a.y);
  printf("X2 = ");
  scanf("%f", &b.x);
  printf("Y2 = ");
  // b.x*=2;
  scanf("%f", &b.y);

  Console_setCursorAttribute(BG_GREEN);

  /*int min = a.x;
  int max = b.x;

  if (b.x >= a.x){
        max = a.x;
        min = b.x;
  }*/


  struct point c = {b.x - a.x, b.y - a.y};
  if (c.y != 0)
    tg = c.x / c.y;
  else
    tg = 0.0;

    if(c.y < 0 && tg > 0)
      tg = -tg;

  /*while (!(min == max && a.y == (int)b.y)) {
    Console_setCursorPosition(min, 2 * ((int)a.x));
    sleepMillis(100);
    printf("  ");
    //if (a.x != b.x)
    min++;
    //if (a.y != b.y)
    a.y += tg;
  }
*/
  for (int i = 0; i < fabs(c.x); i++) {
    Console_setCursorPosition(i - a.x, 2 * ((int)a.y));
    sleepMillis(100);
    printf("  ");
    i++;
    a.y += tg;
  }

  /*struct ConsoleSize console = Console_size();
  for (int i = 0; i <= console.rows; i++) {
    for (int j = 0; j <= console.columns; j++) {
      if (fabs((i - 2 * a.x) * (i - 2 * a.x) + (j - a.y) * (j - a.y) -
               (c.x * c.x + c.y * c.y)) < 19) {
        Console_setCursorPosition(i - 2*(int)a.y, j -  2 * ((int)a.x));
        puts(" ");
      }
    }
  }*/
}
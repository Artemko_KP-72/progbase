#include <assert.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

char *getSpecialString(char *buffer, int bufferLength, const char *str,
					   const char *str2);

int main(void) {
  int bufferLength = 100;
  char buffer[bufferLength];
  printf("%s", buffer);
  char *result =
	  getSpecialString(buffer, bufferLength, "5 3 75326 fsdjf", "12");
  assert(0 == strcmp(result, "3326"));
}

char *getSpecialString(char *buffer, int bufferLength, const char *str,
					   const char *str2) {

  // buffer[0] = '\0';
  int strlength = strlen(str);
  int curr = 0;
  int number = 0;
  int j = 0;

  for (int i = 0; i < strlen(str2); i++) {
	number *= 10;
	number += str2[i] - 48;
  }

  printf("%i\n\n", number);

  for (int i = 0; i < strlength; i++) {
	if (str[i] > '0' && str[i] <= '9') {
	  curr = str[i] - '0';
	  if (number % curr == 0) {
		buffer[j] = str[i];
		j++;
	  }
	}
  }

  buffer[j] = '\0';
  printf("%s", buffer);
  return buffer;
}

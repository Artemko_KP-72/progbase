#include <assert.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define NDEBUG
#define ARRAY_AMOUNT 600
#define _LENGTH(A) sizeof(A) / sizeof(A[0])

struct Paragraph {
  int index;
  char buff[ARRAY_AMOUNT];
};

struct ParagraphArray {
  struct Paragraph *buffpoint;
  int size;
};

char const *ALLOWED_COMMANDS[] = {"task1", "task2", "task3", "task4"};

int fileExists(const char *fileName);
int itemIndex(const char *point);
int getCommandIndex(const char *command);

struct Paragraph firstPar(char *point);
struct ParagraphArray *getAllParagraphsArrayNew(const char *str);

void task1(const char *str);
void task2(char *str);
void task3(const char *str);
void task4(char *filePath);
void arrayFree(struct ParagraphArray *array);
void bufChange(const char *point, char *buffer, int size);

bool isEqualPar(struct Paragraph temp1, struct Paragraph temp2);
bool isEqualParArr(struct ParagraphArray temp1, struct ParagraphArray temp2);

int main(int argc, char *argv[]) {

  int commandIndex = -1;

  if (argc > 1) {
    char *command = argv[1];
    commandIndex = getCommandIndex(command);
  }

  char str[] =
      "1 - First\n\n2 - Second\n\n3 - Third\n\n4 - Fourth\n\n5 - Fifth";

  if (commandIndex == -1 || argc > 3)
    return EXIT_FAILURE;

  if (commandIndex < 4 && argc == 3)
    return EXIT_FAILURE;

  if (commandIndex == 4 && argc == 2)
    return EXIT_FAILURE;

  if (argc == 3) {
    char *filePath = argv[2];
    bool exist = fileExists(filePath);
    if (exist == 0) {
      return EXIT_FAILURE;
    }
    task4(filePath);
  } else {
    if (commandIndex < 4 && argc == 2) {
      if (commandIndex == 1)
        task1(str);
      if (commandIndex == 2)
        task2(str);
      if (commandIndex == 3)
        task3(str);
    }
  }

  return 0;
}

void task1(const char *str) {
  char buffer[] = {"Rose, This is my favorite name... Why?\n\n Cause my first "
                   "love was Rose"};
  char testBuffer[] = {
      "I was created to testing function\n\nAnd i like it\n\nI love my job"};
  char testBuffer2[] = {
      "I love big words\n\nAnd i like spell it\n\nI love to do it"};
  int size = strlen(buffer);

  assert(itemIndex(str) == 11);
  assert(itemIndex(str + 12) + 12 == 23);
  assert(itemIndex(NULL) == -1);
  assert(itemIndex(str + strlen(str)) == -1);

  assert(isEqualPar(firstPar(testBuffer), firstPar(buffer)) == 0);
  bufChange(testBuffer, buffer, size);
  assert(isEqualPar(firstPar(testBuffer), firstPar(buffer)) == 0);
  assert(strcmp("I was created to testing function Why?",
                firstPar(buffer).buff) == 0);
  bufChange(testBuffer2, testBuffer, size);
  assert(strcmp("I love big words testing function",
                firstPar(testBuffer).buff) == 0);
  assert(isEqualPar(firstPar(testBuffer), firstPar(buffer)) == 0);
}

void task2(char *str) {
  struct Paragraph x = {0, "jalhsdfj"};
  struct Paragraph y = {0, "jalhsdfj"};
  struct Paragraph dx = {6, "jalhsdfj"};
  struct Paragraph dy = {1, "jalhvjgsdfj"};
  struct Paragraph comparison = {.index = 0, .buff = "1 - First"};

  assert(isEqualPar(dx, dy) == 0);
  assert(isEqualPar(dy, y) == 0);
  assert(isEqualPar(x, dx) == 0);
  assert(isEqualPar(firstPar(str), x) == 0);
  assert(isEqualPar(x, y) == 1);
  assert(isEqualPar(firstPar(str), comparison) == 1);
  assert(isEqualPar(firstPar(x.buff), x) == 1);
  assert(isEqualPar(firstPar(x.buff), firstPar(y.buff)) == 1);

  struct Paragraph buf[3] = {
      {2, "Buffer filling"}, {15, "Bla - Bla - Bool"}, {300, "(x^2)` = 2*x"}};
  struct ParagraphArray temp1 = {.size = 3, .buffpoint = &buf[0]};
  struct Paragraph buf2[3] = {
      {2, "Buffer filling"}, {15, "Bla - Bla - Bool"}, {300, "(x^2)` = 2*x"}};
  struct ParagraphArray temp2 = {.size = 3, .buffpoint = &buf2[0]};
  struct Paragraph buf3[3] = {{4, "Tra - Ta - Ta "},
                              {15, "Bla - Bla - Bool swimming pool"},
                              {12, "I can in C "}};
  struct ParagraphArray temp3 = {.size = 3, .buffpoint = &buf3[0]};

  assert(isEqualParArr(temp1, temp2) == 1);
  assert(isEqualParArr(temp1, temp3) == 0);
  assert(isEqualPar(firstPar(buf[1].buff), firstPar(buf2[1].buff)) == 1);
}
void task3(const char *str) {
  char thirdTaskArray[] = "Wisdom or sapience\n\n Is the ability to think and "
                          "act using: knowledge\n\n experience\n\n "
                          "understanding\n\n common sense\n\n and insight.";
  struct ParagraphArray *d = getAllParagraphsArrayNew(thirdTaskArray);
  int index = 0;

  //					That's what I'm proud of

  for (int i = 1; i < d->size; i++) {
    d->buffpoint[i].index = 0;
    index += itemIndex(thirdTaskArray + index);
    assert(isEqualPar(firstPar(thirdTaskArray + index), d->buffpoint[i]) == 1);
  }
  arrayFree(d);
}

//                                               FIRST TASK FUNCTIONS
int itemIndex(const char *point) {
  const char *i = point;
  if (i != NULL) {
    int curr = 0;
    for (i = point; *i != '\0'; i++, curr++) {
      if (*(i + 2) != '\0') {
        if (*i == '\n' && *(i + 1) == '\n')
          return curr + 2;
      } else {
        return curr + 1;
      }
    }
  }
  return -1;
}

void bufChange(const char *point, char *buffer, int size) {
  if (point != NULL && buffer != NULL) {
    for (int i = 0; i < size; i++) {
      if (point[i] == '\0' || (point[i] == '\n' && point[i + 1] == '\n'))
        break;
      *(buffer + i) = *(point + i);
    }
  }
}

//                                     SECOND TASK FUNCTIONS
bool isEqualPar(struct Paragraph temp1, struct Paragraph temp2) {
  if (temp1.index == temp2.index && !strcmp(temp1.buff, temp2.buff))
    return true;
  return false;
}

bool isEqualParArr(struct ParagraphArray temp1, struct ParagraphArray temp2) {
  if (temp1.size == temp2.size) {
    for (int i = 0; i < temp1.size; i++) {
      if (isEqualPar(temp1.buffpoint[i], temp2.buffpoint[i]) == 0)
        return false;
    }
    return true;
  }
  return false;
}

struct Paragraph firstPar(char *point) {

  struct Paragraph CurrPar = {.index = 0, .buff = "\0"};

  if (point == NULL) {
    CurrPar.index = -1;
    return CurrPar;
  }
  for (int i = 0; point != NULL && i < ARRAY_AMOUNT; i++) {
    if ((point[i] == '\n' && point[i + 1] == '\n') || (i == ARRAY_AMOUNT - 1)) {
      CurrPar.buff[i] = '\0';
      return CurrPar;
    }
    CurrPar.buff[i] = point[i];
  }
  return CurrPar;
}

//                                     THIRD TASK FUNCTIONS

struct ParagraphArray *getAllParagraphsArrayNew(const char *str) {

  if (str == NULL) {
    return NULL;
  }

  int counter = 0;
  int index = 0;

  for (int i = 0; i < strlen(str); i++) {
    if (str[i + 1] == '\0' || (str[i] == '\n' && str[i + 1] == '\n'))
      counter++;
  }
  struct ParagraphArray *masspoint = malloc(sizeof(struct ParagraphArray));
  masspoint->size = counter;
  masspoint->buffpoint = calloc(sizeof(struct Paragraph), counter);

  for (int i = 0; i < counter; i++) {
    bufChange(str + index, masspoint->buffpoint[i].buff, ARRAY_AMOUNT);
    index += itemIndex(str + index);
  }
  return masspoint;
}

// remember *(name).data == name->data
void arrayFree(struct ParagraphArray *array) {
  if (array->buffpoint != NULL)
    free(array->buffpoint);
  free(array);
}

int getCommandIndex(const char *command) {
  for (int i = 0; i < _LENGTH(command); i++) {
    if (strcmp(command, ALLOWED_COMMANDS[i]) == 0) {
      return i + 1;
    }
  }
  return -1;
}
//					FILE FUNCTION
int fileExists(const char *fileName) {
  FILE *f = fopen(fileName, "rb");
  if (!f)
    return 0; // false: not exists
  fclose(f);
  return 1; // true: exists
}

long getFileSize(const char *fileName) {
  FILE *f = fopen(fileName, "rb");
  if (!f)
    return -1;           // error opening file
  fseek(f, 0, SEEK_END); // rewind cursor to the end of file
  long fsize = ftell(f); // get file size in bytes
  fclose(f);
  return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength) {
  FILE *f = fopen(fileName, "rb");
  if (!f)
    return 0; // read 0 bytes from file
  long readBytes = fread(buffer, 1, bufferLength, f);
  fclose(f);
  return readBytes; // number of bytes read
}
//							FILE WORKING FUNCTIONS
void task4(char *filePath) {
  if (fileExists(filePath) == 1) {
    long n = getFileSize(filePath);
    char textBuffer[n + 1];
    textBuffer[n] = '\0';
    readFileToBuffer(filePath, textBuffer, n);
    struct ParagraphArray *d = getAllParagraphsArrayNew(textBuffer);
    puts("\n                                                                  "
         "I hava a Paragraph task\n");
    puts("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-"
         "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n");
    for (int i = 0; i < d->size; i++) {
      int count = 0;

      for (int j = 0; d->buffpoint[i].buff[j] != '\0'; j++) {
        count++;
      }
      if (count < 100)
        Console_setCursorAttribute(FG_RED);
      if (count >= 100 && count < 150)
        Console_setCursorAttribute(FG_YELLOW);
      if (count >= 150)
        Console_setCursorAttribute(FG_GREEN);
      printf("%s\n\n", d->buffpoint[i].buff);
      Console_setCursorAttribute(FG_DEFAULT);
    }
    puts("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-"
         "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n");

    for (int i = 0; i < d->size; i++) {
      int count = 0;

      for (int j = 0; d->buffpoint[i].buff[j] != '\0'; j++) {
        count++;
      }
      if (count >= 100)
        printf("%s\n\n", d->buffpoint[i].buff);
    }
    arrayFree(d);
  } else {
  }
}
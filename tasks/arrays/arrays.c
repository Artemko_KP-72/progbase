#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
  int A[10];
  char S[10];
  int sum = 0;
  int count = 0;
  int imin = 50;
  int imax = -50;
  int min = 0;
  int max = 0;
  double demp = 0;
  srand(time(0));

  Console_clear();

  for (int i = 0; i < 10; i++) {
    Console_setCursorAttribute(BG_WHITE);
    Console_setCursorAttribute(FG_BLACK);
    A[i] = rand() % 300 - 99;
    printf("%3d   ", A[i]);
  }

  Console_setCursorAttribute(BG_DEFAULT);
  Console_setCursorAttribute(FG_DEFAULT);

  puts("");

  for (int i = 9; i > -1; i--) {
    printf("%3d   ", A[i]);
  }
  puts("");

  for (int i = 0; i < 10; i++) {
    if (A[i] > 100) {
      Console_setCursorAttribute(BG_BLUE);
    }
    printf("%3i", A[i]);
    Console_setCursorAttribute(BG_DEFAULT);
    printf("   ");
  }
  puts("");
  
  for (int i = 0; i < 10; i++) {
    if (A[i] > 127) {
      Console_setCursorAttribute(BG_RED);
      count++;
      sum = sum + A[i];
    }
    demp = (float)sum / count;
    printf("%3i", A[i]);
    Console_setCursorAttribute(BG_DEFAULT);
    printf("   ");
  }

  puts(" ");
  
  if (count != 0) {
    printf("\ncount = %i\nsum = %i\naverage meaning = %lf", count, sum, demp);
  } else {
    puts("\nAverage meaning is impossible to calculate!!!");
  }
  puts("");

  for (int i = 0; i < 10; i++) {
    if (abs(A[i]) <= 50) {
      if (A[i] <= min) {
        min = A[i];
        imin = i;
      }
      if (A[i] >= max) {
        max = A[i];
        imax = i;
      }
    }
  }
  puts(" ");
  printf("max Array = %3i   sequance number is %i\nmin Array = %3i   "
         "sequance number is %i\n",
         max, imax + 1, min, imin + 1);

         for( int i = 0; i < 10; i++){
             S[i] = (A[i] % 95) + 32 ;
            }
         S[9]= '\0';
         puts(" ");
         puts(S);
         puts("");

    Console_setCursorAttribute(BG_YELLOW);
    Console_setCursorAttribute(FG_BLACK);

         for( int i = 0; i < 10; i++){
             if (((A[i]) <= 25 ) && (A[i] >= - 25)) {
            A[i] = 0 ;
             }
             printf("%2i  ", A[i]);
           }
           puts("");
           Console_setCursorAttribute(BG_DEFAULT);
}
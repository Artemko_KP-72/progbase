#include <assert.h>
#include <ctype.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*
gcc main.c -std=c11 -Wall -Werror -pedantic-errors -lm -lprogbase
*/

char const *ALLOWED_COMMANDS[] = {"-k", "-o", "-d", "-f"};

void getCommandIndex(char *argv, bool *k, bool *f, bool *d, bool *o);
void dechyper();

bool fturn(char *argv, char *toEncode);

int fileExists(const char *fileName);
long getFileSize(const char *fileName);
int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);

int main(int argc, char *argv[]) {

  bool k = false;
  bool o = false;
  bool d = false;
  bool f = false;
  bool newO = false;

  int mod = 0;

  char current = '\0';
  char *toEncode = NULL;
  char *chiper = NULL;

  FILE *fileOutPut = NULL;

  srand(time(NULL));

  for (int i = 0; i < argc; i++) {

    for (int j = 0; j < strlen(argv[i]); j++) {
      argv[i][j] = tolower(argv[i][j]);
    }
  }

  for (int i = 0; i < argc; i++) {

    if (i != argc - 1) {
      if (argv[i][0] == '-' && argv[i + 1][0] == '-' && argv[i][1] != 'd' &&
          argv[i + 1][1] != 'd')
        return EXIT_FAILURE;
    }

    if (argv[i][0] == '-') {
      getCommandIndex(argv[i], &k, &f, &d, &o);

      if (k == true) {
        i++;
        chiper = argv[i];
        k = false;
      }

      if (f == true) {
        i++;
        if (!fturn(argv[i], toEncode))
          return EXIT_FAILURE;
        f = false;
      }

      if (o == true) {
        i++;
        fileOutPut = fopen(argv[i], "w");
        if (!fileOutPut) {
          printf("This output file does not exist\n");
          return EXIT_FAILURE;
        }
        newO = true;
        o = false;
      }
    } else {
      toEncode = argv[i];
    }
  }

  for (int i = 0; i < strlen(toEncode); i++) {
    toEncode[i] = tolower(toEncode[i]);
  }

  if (!strcmp(chiper, "random")) {
    int count = rand() % 10 + 3;
    for (int i = 3; i < count; i++) {
      chiper[i - 3] = rand() % 27 + 96;
      chiper[i + 1] = '\0';
    }
  }

  mod = strlen(chiper);

  if (newO == false)
    printf("%s\n", chiper);
  else
    fprintf(fileOutPut, "%s\n", chiper);

  for (int i = 0; i < strlen(toEncode); i++) {
    if (isalpha(toEncode[i]) && isalpha(chiper[i % mod])) {
      if (d == true) {
        current =
            96 + ((toEncode[i] - 96) - (chiper[i % (mod)] - 96 - 1) + 26) % 27;
      } else {
        current = 96 + ((toEncode[i] - 96) + (chiper[i % (mod)] - 96 - 1)) % 26;
      }
      if (current == '`')
        current = 'z';
      if (newO == false) {
        printf("%c", current);
      } else {
        fprintf(fileOutPut, "%c", current);
      }

    } else {
      if (newO == false) {
        printf("%c", toEncode[i]);
      } else {
        fprintf(fileOutPut, "%c", toEncode[i]);
      }
    }
  }
  newO = false;
  if (o == true)
    fclose(fileOutPut);
  return EXIT_SUCCESS;
}

void getCommandIndex(char *argv, bool *k, bool *f, bool *d, bool *o) {

  switch (argv[1]) {

  case 'k':
    *k = true;
    break;

  case 'f':
    *f = true;
    break;

  case 'd':
    *d = true;
    break;

  case 'o':
    *o = true;
    break;
  }
}

bool fturn(char *argv, char *toEncode) {
  if (fileExists(argv)) {
    FILE *fileInPut = fopen(argv, "rb"); // read 0 bytes from file
    int len = getFileSize(argv);
    readFileToBuffer(argv, toEncode, len);
    toEncode[len] = '\0';
    fclose(fileInPut);
  } else {
    printf("This input file does not exist\n");
    return false;
  }
  return true;
}

int fileExists(const char *fileName) {
  FILE *f = fopen(fileName, "rb");
  if (!f)
    return 0; // false: not exists
  fclose(f);
  return 1; // true: exists
}

long getFileSize(const char *fileName) {
  FILE *f = fopen(fileName, "rb");
  if (!f)
    return -1;           // error opening file
  fseek(f, 0, SEEK_END); // rewind cursor to the end of file
  long fsize = ftell(f); // get file size in bytes
  fclose(f);
  return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength) {
  FILE *f = fopen(fileName, "rb"); // read 0 bytes from file
  long readBytes = fread(buffer, 1, bufferLength, f);
  fclose(f);
  return readBytes; // number of bytes read
}
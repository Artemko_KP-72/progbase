#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int hexaAmount(const char *const str, const int amount);
int hexaskip(const char *firstChar, const int amount);
int count(const char *str, int counter);
void printDown(const char *str);

int main(void) {
  char str[100];
  
  printf("Please, enter a string: ");
  fgets(str, 100, stdin);
  printf("You've entered: ");
  printDown(str);
  printf("You've entered: %i", count(str, 0) - 1);
  printf(" symbols\n");
  printf("Calculate and output the number of hexadecimal characters in line in the console: %i\n", hexaAmount(str, 0));

  return EXIT_SUCCESS;
}

int hexaAmount(const char *const str,const int amount) {
  const char firstChar = str[0];
  if (firstChar == '\0')
    return amount;
  if ((firstChar < 65 || firstChar > 70) && (firstChar < 48 || firstChar > 57))
    return hexaAmount(str + 1, amount);
  else
    return hexaskip(str + 1, amount + 1);
}

int hexaskip(const char *firstChar, const int amount) {
  if (*firstChar == '\0')
    return amount;
  if ((*firstChar > 64 && *firstChar < 71) ||
      (*firstChar > 47 && *firstChar < 58))
    return hexaskip(firstChar + 1, amount);
  else
    return hexaAmount(firstChar + 1, amount);
}

int count(const char *str, int counter) {
  if (str[0] == '\0')
    return counter;
  return count(str + 1, counter + 1);
}

void printDown(const char *str) {
  if (str[0] == '\0')
    return;
  putchar(str[0]);
  printDown(str + 1);
}

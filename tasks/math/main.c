#include <stdio.h>
#include <math.h>

    int main(void) {
        
        double x  = -10.0;
          double F1 =   0.0;
        double F2 =   0.0;

        for(int i = 1; i < 42; i = i + 1) {

            printf("X = %lf\n", x);
            F1 =  3 - pow( x , 2 );
            printf("F1 = %lf\n", F1);
            if (cos( x + 2 ) == 0){
                puts("F2 cannot be computed");
            }
            else{
            F2 = tan( x + 2 ) * 0.5;

            printf("F2 = %lf\n", F2);
            printf("F1 + F2 = %lf\n", F1 + F2 );
            printf("F1 * F2 = %lf\n", F1 * F2 );

                if (F2 == 0.0)
                    puts("Division by zero\n");
                               else 
                    printf("F1 / F2 = %lf\n", F1 / F2);
                if (F1 == 0.0)
                    puts("Division by zero\n");
                else 
                    printf("F2 / F1 = %lf\n", F2 / F1);
            }
            puts("------------------------");

            x = x + 0.5;
        }

        return 0;
    }
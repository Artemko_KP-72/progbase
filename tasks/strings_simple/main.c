#include <limits.h>
#include <math.h>
#include <progbase/console.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
/*
gcc main.c -std=c11 -Wall -Werror -pedantic-errors -lm -lprogbase
*/
int main(void) {

  int counter = 0;
  int pseudodelta = 60;
  int delta = pseudodelta;
  int losses = 0;
  int c = 0;

  Console_clear();

  char hugearray[] =
      "I`ve watched through his eyes, I`ve listened through his ears, and I "
      "tell you he`s the one. Or at least as close as we`re going to get. "
      "That`s what you said about the brother. Or at least as close as we`re "
      "going to get. That`s what you said about the brother. The brother "
      "tested out impossible. For other reasons. Nothing to do with his "
      "ability. Same with the sister. And there are doubts about him. He`s too "
      "malleable. Too willing to submerge himself in someone else`s will. Not "
      "if the other person is his enemy. So what do we do? Surround him with "
      "enemies all the time? If we have to. I thought you said you liked this "
      "kid. If the buggers get him, they`ll make me look like his favorite "
      "uncle. All right. We`re saving the world, after all. Take him.";


  puts("");
  Console_setCursorAttribute(BG_BLUE);
  puts("Task One");
  Console_reset();
  puts("");

  for (int i = 0; i < strlen(hugearray); i++) {
    putchar(hugearray[i]);
    if ((hugearray[i] == ' ') && (i / (delta + counter) > 1)) {
      counter = counter + delta;
      puts("");
      putchar(' ');
    }
  }
  counter = 0;
  delta = pseudodelta;

  printf(
      "\n\n==========================================\n\n Number of characters "
      "is %i\n\n==========================================\n\n",
      strlen(hugearray));

  Console_setCursorAttribute(BG_BLUE);
  puts("Task Two");
  Console_reset();
  puts("");

  for (int i = 0; i < strlen(hugearray); i++) {

    if ((hugearray[i] != ',') && (hugearray[i] != '.')) {
      putchar(hugearray[i]);
      if ((hugearray[i] == ' ') && (i / (delta + counter) > 1)) {
        counter = counter + delta;
        puts("");
      }
    } else {
      losses++;
    }
  }
  printf("\n\n==========================================\n\n Number of "
         "characters is %i\n\n==========================================\n\n",
         strlen(hugearray) - losses);

  counter = 0;

  Console_setCursorAttribute(BG_BLUE);
  puts("Task Tree");
  Console_reset();
  puts("");
  for (int i = 0; i < strlen(hugearray); i++) {
    putchar(hugearray[i]);
    counter++;
    if (hugearray[i] == '.') {
      Console_setCursorPosition(i, 100);
      printf("   Number of characters in the sentence is %i\n", counter);
      counter = 0;
    }
  }

  printf("\n==========================================\n\n");

  Console_setCursorAttribute(BG_BLUE);
  puts("Task Four");
  Console_reset();
  puts("");

  for (int i = 0; i < strlen(hugearray); i++) {
    if (hugearray[i] == ' ')
      counter++;
  }
  printf("Total number of words is %i\n", counter);
  counter = 0;

  puts("\n==========================================\n");
  Console_setCursorAttribute(BG_BLUE);
  puts("Task Five");
  Console_reset();
puts("");

  for (int i = 0; i < strlen(hugearray); i++) {
    if (hugearray[i] == 'I') {
      putchar(',');
      putchar(' ');
      counter++;
    }
    if (((hugearray[i] == ' ') || (hugearray[i] == '`')) &&
        ((hugearray[i + 3] == ' ') ||
         ((hugearray[i + 2] == ' ') || ((hugearray[i + 4] == ' '))))) {
      do {
        i++;
        putchar(hugearray[i]);
      } while (hugearray[i + 1] != ' ');
      putchar(',');
      putchar(' ');
      counter++;
    }
  }
  printf("\n\nTotal number of this kind of words is %i", counter);
  puts("\n\n==========================================\n");

  Console_setCursorAttribute(BG_BLUE);
  puts("Task Six");
  Console_reset();
  puts("");

  for (int i = 0; i < strlen(hugearray); i++) {
    if ((hugearray[i - 1] == ' ') &&
        ((hugearray[i] == 'a') || (hugearray[i] == 'I') ||
         (hugearray[i] == 'o') || (hugearray[i] == 'A') ||
         (hugearray[i] == 'e'))) {
      c = 0;
      do {
        putchar(hugearray[i + c]);
        c++;
      } while (hugearray[i + c] != ' ');
      putchar(' ');
      putchar(',');
    }
  }
  puts("");
}
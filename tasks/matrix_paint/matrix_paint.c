#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>

int main(void) {
    /* colors encoding table 
        BG_BLACK = 40,
        BG_RED,
        BG_GREEN,
        BG_YELLOW,
        BG_BLUE,
        BG_MAGENTA,
        BG_CYAN,
        BG_WHITE,
        ,BG_INTENSITY_BLACK = 100,
        BG_INTENSITY_RED,
        BG_INTENSITY_GREEN,
        BG_INTENSITY_YELLOW,
        BG_INTENSITY_BLUE,
        BG_INTENSITY_MAGENTA,
        BG_INTENSITY_CYAN,
    BG_INTENSITY_WHITE*/
    const char colorsTable[16][2] = {

        {'0', BG_BLACK},
        {'1', BG_INTENSITY_BLACK},
        {'2', BG_RED},
        {'3', BG_INTENSITY_RED},
        {'4', BG_GREEN}, //Green
        {'5', BG_INTENSITY_YELLOW},
        {'6', BG_YELLOW},//Yellow
        {'7', BG_INTENSITY_WHITE},
        {'8', BG_MAGENTA},
        {'9', BG_INTENSITY_MAGENTA},
        {'A', BG_MAGENTA},//Magenta
        {'B', BG_INTENSITY_CYAN},
        {'C', BG_BLUE},//Blue
        {'D', BG_INTENSITY_BLUE},//intensity blue
        {'E', BG_WHITE},
        {'F', BG_INTENSITY_WHITE}
    };
    int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    char colorsPalette[] = "0123456789ABCDEF";
    int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
    int i = 0;
    int colorPairIndex = 0;
    Console_clear();
    for (i = 0; i < colorsPaletteLength; i++)
    {
        char colorCode = '\0';
        char color = '\0';
        /* get current color code from colorsPalette */
        colorCode = colorsPalette[i];
        /* find corresponding color in table */
        for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) 
        {
            char colorPairCode = colorsTable[colorPairIndex][0];
            char colorPairColor = colorsTable[colorPairIndex][1];
            if (colorCode == colorPairCode) 
            {
                color = colorPairColor;
                break;  /* we have found our color, break the loop */
            }
        }
        /* print space with founded color background */
        Console_setCursorAttribute(color);
        putchar(' ');
    }
    puts("");
    Console_reset();
    char image[28][28] = {
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2' },
        { '2','2','2','2','2','2','2','2','2','2','2','2','7','7','7','7','2','2','2','2','2','2','2','2','2','2','2','2' },
        { '3','3','3','3','3','3','3','3','3','3','7','7','7','7','7','7','7','7','3','3','3','3','3','3','3','3','3','3' },
        { '3','3','3','3','3','3','3','3','3','7','7','7','7','7','7','7','7','7','7','3','3','3','3','3','3','3','3','3' },
        { '6','6','6','6','6','6','6','6','7','7','F','0','7','7','7','7','F','0','7','7','6','6','6','6','6','6','6','6' },
        { '6','6','6','6','6','6','6','6','7','7','0','0','7','7','7','7','0','0','7','7','6','6','6','6','6','6','6','6' },
        { '4','4','4','4','4','4','4','7','7','7','7','7','7','7','7','7','7','7','7','7','7','4','4','4','4','4','4','4' },
        { '4','4','4','4','4','4','4','7','7','7','7','7','7','7','7','7','7','7','7','7','7','4','4','4','4','4','4','4' },
        { 'D','D','D','D','D','D','D','7','7','7','7','7','0','7','0','7','7','7','7','7','7','D','D','D','D','D','D','D' },
        { 'D','D','D','D','D','D','D','D','7','7','7','7','7','7','7','7','7','7','7','7','D','D','D','D','D','D','D','D' },
        { 'D','D','D','D','D','D','D','D','7','7','7','2','7','7','7','2','7','7','7','7','D','D','D','D','D','D','D','D' },
        { 'C','C','C','C','C','C','C','C','C','7','7','7','2','2','2','7','7','7','7','C','C','C','C','C','C','C','C','C' },
        { 'C','C','C','C','C','C','C','C','C','C','7','7','7','7','7','7','7','7','C','C','C','C','C','C','C','C','C','C' },
        { 'A','A','A','A','A','A','A','A','A','A','A','7','7','7','7','7','7','A','A','A','A','A','A','A','A','A','A','A' },
        { 'A','A','A','A','A','A','A','A','A','A','A','A','7','7','7','7','A','A','A','A','A','A','A','A','A','A','A','A' },
        { 'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' }
    };
        for (int i = 0; i <= 27; i++){
            for(int k = 0; k <= 27; k++){        
                for(int j = 0; j <= 15; j++)
                    if(colorsTable[j][0] == image[i][k]){
                        Console_setCursorAttribute(colorsTable[j][1]);
                        putchar(' ');                      
                    }
                }
                puts("");
        }

Console_setCursorPosition(1,31);
        for (int i = 27; i >= 0; i--){
            for(int k = 27; k >= 0; k--){        
                for(int j = 0; j <= 15; j++)
                    if(colorsTable[j][0] == image[i][k]){
                        Console_setCursorAttribute(colorsTable[j][1]);
                        Console_setCursorPosition(29 - i,60 - k);    
                        putchar(' ');                  
                    }
                }
        }

        for (int i = 0; i <= 27 ; i++){
            if (i % 2 == 0){
                for ( int j = 27; j >= 0; j--){


                }
            }
        }
        for (int j = 0; j < 28; j++){
            for (i = (j % 2 !=0) ? 27 : 0; i >= 0 && i < 28; i = (j % 2 !=0) ? (i - 1) : (i + 1)){
                for (int k = 0; k < 16; k++){
                    if (image[i][j] == colorsTable[k][0]){
                        Console_setCursorAttribute (colorsTable[k][1]);
                        break;
                    }
                }
                Console_setCursorPosition(i + 2,j + 70);
                sleepMillis(15);
                putchar (' ');
                Console_reset();
            }
        }
        puts("");

   
   
        /*
            for (int j = 0; j < 28; j++){
            for (int i = (j % 2 !=0) ? 27 : 0; i >= 0 && i < 28; i = (j % 2 !=0) ? (i - 1) : (i + 1)){
                for (int k = 0; k < 16; k++){
                    if (image[i][j] == colorsTable[k][0]){
                        Console_setCursorAttribute (colorsTable[k][1]);
                        break;
                    }
                }
                Console_setCursorPosition(i + 2,j + 31);
                sleepMillis(delay);
                putchar (' ');
                Console_reset();
            }
        }
  */

        Console_reset();
        Console_setCursorPosition(1, 70);
    return 0;
}